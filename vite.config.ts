import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { VitePWA } from 'vite-plugin-pwa'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(), 
    VitePWA({
      registerType: 'autoUpdate',
      devOptions: {
        enabled: true
      },
      manifest: {
        name: 'Text To Anamorphosis',
        short_name: 'Text To Ana',
        description: 'Transforme deux mots en art 3D',
        theme_color: '#ffffff',
        icons: [
          {
            src: "/icon512.png",
            sizes: "512x512",
            type: "image/png"
          }
        ]},
      workbox: {
      }
    }),
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  assetsInclude: ['**/*.gltf', '**/*.glb'],
})
