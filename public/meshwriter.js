/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*!
 * Babylon MeshWriter
 * https://github.com/briantbutton/meshwriter
 * (c) 2018-2021 Brian Todd Button
 * Released under the MIT license
 */


// *-*=*  *=*-* *-*=*  *=*-* *-*=*  *=*-* *-*=*  *=*-* *-*=*  *=*-* *-*=*  *=*-*
// This function loads the specific type-faces and returns the superconstructor
// If BABYLON is loaded, it assigns the superconstructor to BABYLON.MeshWriter
// Otherwise it assigns it to global variable 'BABYLONTYPE'
// 
// Note to developers:  Helvetica Neue Medium is assumed, by the code, to be present
//                      Do NOT remove it during customization

!(__WEBPACK_AMD_DEFINE_ARRAY__ = [
  // './fonts/helveticaneue-medium',
__webpack_require__(2)], __WEBPACK_AMD_DEFINE_RESULT__ = (function(RMR){
// >>>>>  STEP 1 <<<<<

  var   scene,FONTS,defaultColor,defaultOpac,naturalLetterHeight,curveSampleSize,Γ=Math.floor,rmr,debug;
  var   b128back,b128digits;
  var   earcut                 = __webpack_require__(3);
  var   B                      = {},
        methodsList            = ["Vector2","Vector3","Path2","Curve3","Color3","SolidParticleSystem","PolygonMeshBuilder","CSG","StandardMaterial","Mesh"];
  prepArray();
  // >>>>>  STEP 2 <<<<<
  // hnm                          = HNM(codeList);                         // Do not remove
  rmr                          = RMR(codeList);
  // >>>>>  STEP 2 <<<<<
  FONTS                        = {};
  // >>>>>  STEP 3 <<<<<
  // FONTS["HelveticaNeue-Medium"]= hnm;                                   // Do not remove
  // FONTS["Helvetica"]           = hnm;
  FONTS["RubikMonoOne-Regular"]= rmr;
  // >>>>>  STEP 3 <<<<<
  defaultColor                 = "#808080";
  defaultOpac                  = 1;
  curveSampleSize              = 6;
  naturalLetterHeight          = 1000;

  // *-*=*  *=*-* *-*=*  *=*-* *-*=*  *=*-* *-*=*  *=*-* *-*=*  *=*-* *-*=*  *=*-*
  //  SUPERCONSTRUCTOR  SUPERCONSTRUCTOR  SUPERCONSTRUCTOR 
  // Parameters:
  //   ~ scene
  //   ~ preferences

  var Wrapper                  = function(){

    var proto,defaultFont,scale,meshOrigin,preferences;

    scene                      = arguments[0];
    preferences                = makePreferences(arguments);

    defaultFont                = isObject(FONTS[preferences.defaultFont]) ? preferences.defaultFont : "RubikMonoOne-Regular";
    meshOrigin                 = preferences.meshOrigin==="fontOrigin" ? preferences.meshOrigin : "letterCenter";
    scale                      = isNumber(preferences.scale) ? preferences.scale : 1;
    debug                      = isBoolean(preferences.debug) ? preferences.debug : false;

    // *-*=*  *=*-* *-*=*  *=*-* *-*=*  *=*-* *-*=*  *=*-* *-*=*  *=*-* *-*=*  *=*-*
    //  CONSTRUCTOR  CONSTRUCTOR  CONSTRUCTOR  CONSTRUCTOR
    // Called with 'new'
    // Parameters:
    //   ~ letters
    //   ~ options

    function MeshWriter(lttrs,opt){

      var material,meshesAndBoxes,offsetX,meshes,lettersBoxes,lettersOrigins,combo,sps,mesh,xWidth;

      //  ~  -  =  ~  -  =  ~  -  =  ~  -  =  ~  -  =  ~  -  =  
      // Here we set ALL parameters with incoming value or a default
      // setOption:  applies a test to potential incoming parameters
      //             if the test passes, the parameters are used, else the default is used
      var options              = isObject(opt) ? opt : { } ,
          position             = setOption ( options,  "position", isObject, {} ) ,
          colors               = setOption ( options,  "colors",   isObject, {} ) ,
          fontFamily           = setOption ( options,  "font-family", isSupportedFont, defaultFont ) ,
          anchor               = setOption ( options,  "anchor",   isSupportedAnchor, "left" ) ,
          rawheight            = setOption ( options,  "letter-height", isPositiveNumber, 100 ) ,
          rawThickness         = setOption ( options,  "letter-thickness", isPositiveNumber, 1 ) ,
          basicColor           = setOption ( options,  "color",    isString, defaultColor ) ,
          opac                 = setOption ( options,  "alpha",    isAmplitude, defaultOpac ) ,
          y                    = setOption ( position, "y",        isNumber, 0),
          x                    = setOption ( position, "x",        isNumber, 0),
          z                    = setOption ( position, "z",        isNumber, 0),
          diffuse              = setOption ( colors,   "diffuse",  isString, "#F0F0F0"),
          specular             = setOption ( colors,   "specular", isString, "#000000"),
          ambient              = setOption ( colors,   "ambient",  isString, "#F0F0F0"),
          emissive             = setOption ( colors,   "emissive", isString, basicColor),
          fontSpec             = FONTS[fontFamily],
          letterScale          = round(scale*rawheight/naturalLetterHeight),
          thickness            = round(scale*rawThickness),
          letters              = isString(lttrs) ? lttrs : "" ;

      //  ~  -  =  ~  -  =  ~  -  =  ~  -  =  ~  -  =  ~  -  =  
      // Now all the parameters are set, let's get to business
      // First create the material
      material                 = makeMaterial(scene, letters, emissive, ambient, specular, diffuse, opac);

      //  ~  -  =  ~  -  =  ~  -  =  ~  -  =  ~  -  =  ~  -  =  
      // Next, create the meshes
      // This creates an array of meshes, one for each letter
      // It also creates two other arrays, which are used for letter positioning
      meshesAndBoxes           = constructLetterPolygons(letters, fontSpec, 0, 0, 0, letterScale, thickness, material, meshOrigin);
      meshes                   = meshesAndBoxes[0];
      lettersBoxes             = meshesAndBoxes[1];
      lettersOrigins           = meshesAndBoxes[2];
      xWidth                   = meshesAndBoxes.xWidth;           

      //  ~  -  =  ~  -  =  ~  -  =  ~  -  =  ~  -  =  ~  -  =  
      // The meshes are converted into particles of an SPS
      combo                    = makeSPS(scene, meshesAndBoxes, material);
      sps                      = combo[0];
      mesh                     = combo[1];

      //  ~  -  =  ~  -  =  ~  -  =  ~  -  =  ~  -  =  ~  -  =  
      // Set the final SPS-mesh position according to parameters
      offsetX                  = anchor==="right" ? (0-xWidth) : ( anchor==="center" ? (0-xWidth/2) : 0 );
      mesh.position.x          = scale*x+offsetX;
      mesh.position.y          = scale*y;
      mesh.position.z          = scale*z;

      this.getSPS              = ()  => sps;
      this.getMesh             = ()  => mesh;
      this.getMaterial         = ()  => material;
      this.getOffsetX          = ()  => offsetX;
      this.getLettersBoxes     = ()  => lettersBoxes;
      this.getLettersOrigins   = ()  => lettersOrigins;
      this.color               = c   => isString(c)?color=c:color;
      this.alpha               = o   => isAmplitude(o)?opac=o:opac;
      this.clearall            = function()  {sps=null;mesh=null;material=null};
    };
    //  CONSTRUCTOR  CONSTRUCTOR  CONSTRUCTOR  CONSTRUCTOR
    // *-*=*  *=*-* *-*=*  *=*-* *-*=*  *=*-* *-*=*  *=*-* *-*=*  *=*-* *-*=*  *=*-*

    proto                      = MeshWriter.prototype;

    proto.setColor             = function(color){
      var material             = this.getMaterial();
      if(isString(color)){
        material.emissiveColor = rgb2Bcolor3(this.color(color));
      }
    };
    proto.setAlpha             = function(alpha){
      var material             = this.getMaterial();
      if(isAmplitude(alpha)){
        material.alpha         = this.alpha(alpha)
      }
    };
    proto.overrideAlpha        = function(alpha){
      var material             = this.getMaterial();
      if(isAmplitude(alpha)){
        material.alpha         = alpha
      }
    };
    proto.resetAlpha           = function(){
      var material             = this.getMaterial();
      material.alpha           = this.alpha()
    };
    proto.getLetterCenter      = function(ix){
      return new B.Vector2(0,0)
    }
    proto.dispose              = function(){
      var mesh                 = this.getMesh(),
          sps                  = this.getSPS(),
          material             = this.getMaterial();
      if(sps){sps.dispose()}
      this.clearall()
    };
    MeshWriter.codeList        = codeList;
    MeshWriter.decodeList      = decodeList;

    return MeshWriter;
  };
  if ( typeof window !== "undefined" ) {
    window.TYPE                = Wrapper;
    window.MeshWriter          = Wrapper
  }
  if ( typeof global !== "undefined" ) {
    global.MeshWriter          = Wrapper
  }
  if ( typeof BABYLON === "object" ) {
    cacheMethods(BABYLON);
    BABYLON.MeshWriter         = Wrapper;
  };
  if (  true && module.exports ) {
    module.exports             = Wrapper;
  }
  return Wrapper;

  //  ~  -  =  ~  -  =  ~  -  =  ~  -  =  ~  -  =  ~  -  =  ~  -  =
  // MakeSPS turns the output of constructLetterPolygons into an SPS
  // with the whole string, with appropriate offsets
  function makeSPS(scene,meshesAndBoxes,material){
    var meshes                 = meshesAndBoxes[0],
        lettersOrigins         = meshesAndBoxes[2],sps,spsMesh;
    if(meshes.length){
      sps                      = new B.SolidParticleSystem("sps"+"test",scene, { } );
      meshes.forEach(function(mesh,ix){
        sps.addShape(mesh, 1, {positionFunction: makePositionParticle(lettersOrigins[ix])});
        mesh.dispose()
      });
      spsMesh                  = sps.buildMesh();
      spsMesh.material         = material;
      sps.setParticles()
    }
    return [sps,spsMesh];

    function makePositionParticle(letterOrigins){
      return function positionParticle(particle,ix,s){
        particle.position.x    = letterOrigins[0]+letterOrigins[1];
        particle.position.z    = letterOrigins[2]
      }
    }
  };

  //  ~  -  =  ~  -  =  ~  -  =  ~  -  =  ~  -  =  ~  -  =  ~  -  =  ~  -  =
  // Takes specifications and returns an array with three elements, each of which
  // is an array (length of each array === the number of incoming characters)
  //   ~ the meshes (not offset by position)
  //   ~ the boxes (to help with positions features) 
  //   ~ the letter origins (providing offset for each letter)
  function constructLetterPolygons(letters, fontSpec, xOffset, yOffset, zOffset, letterScale, thickness, material, meshOrigin){
    var letterOffsetX          = 0,
        lettersOrigins         = new Array(letters.length),
        lettersBoxes           = new Array(letters.length),
        lettersMeshes          = new Array(letters.length),
        ix                     = 0, letter, letterSpec, lists, shapesList, holesList, letterMeshes, letterBox, letterOrigins, meshesAndBoxes, i;

    for(i=0;i<letters.length;i++){
      letter                   = letters[i];
      letterSpec               = makeLetterSpec(fontSpec,letter);
      if(isObject(letterSpec)){
        lists                  = buildLetterMeshes(letter, i, letterSpec, fontSpec.reverseShapes, fontSpec.reverseHoles);
        shapesList             = lists[0];
        holesList              = lists[1];
        letterBox              = lists[2];
        letterOrigins          = lists[3];

        // ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~
        // This subtracts the holes, if any, from the shapes and merges the shapes
        // (Many glyphs - 'i', '%' - have multiple shapes)
        // At the end, there is one mesh per glyph, as God intended
        letterMeshes           = punchHolesInShapes(shapesList,holesList);
        if(letterMeshes.length){
          lettersMeshes[ix]    = merge(letterMeshes);
          lettersOrigins[ix]   = letterOrigins;
          lettersBoxes[ix]     = letterBox;
          ix++
        }
      }
    };
    meshesAndBoxes             = [lettersMeshes,lettersBoxes,lettersOrigins];
    meshesAndBoxes.xWidth      = round(letterOffsetX);
    meshesAndBoxes.count       = ix;
    return meshesAndBoxes;

    //  ~  -  =  ~  -  =  ~  -  =  ~  -  =  ~  -  =  ~  -  =  ~  -  =  ~  -  =
    // A letter may have one or more shapes and zero or more holes
    // The shapeCmds is an array of shapes
    // The holeCmds is an array of array of holes, the outer array lining up with
    // the shapes array and the inner array permitting more than one hole per shape
    // (Think of the letter 'B', with one shape and two holes, or the symbol
    // '%' which has three shapes and two holes)
    // 
    // For mystifying reasons, the holeCmds (provided by the font) must be reversed
    // from the original order and the shapeCmds must *not* be reversed
    // UNLESS the font is Jura, in which case the holeCmds are not reversed
    // (Possibly because the Jura source is .otf, and the others are .ttf)
    //
    // *WARNING*                                                         *WARNING*
    // buildLetterMeshes performs a lot of arithmetic for offsets to support
    // symbol reference points, BABYLON idiocyncracies, font idiocyncracies,
    // symbol size normalization, the way curves are specified and "relative"
    // coordinates.  (Fonts use fixed coordinates but many other SVG-style
    // symbols use relative coordinates)
    // *WARNING*                                                         *WARNING*
    //  ~  -  =  ~  -  =  ~  -  =  ~  -  =  ~  -  =  ~  -  =  ~  -  =  ~  -  =

    function buildLetterMeshes(letter, index, spec, reverseShapes, reverseHoles){

      // ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~
      // A large number of offsets are created, per warning
      var balanced             = meshOrigin === "letterCenter",
          centerX              = (spec.xMin+spec.xMax)/2,
          centerZ              = (spec.yMin+spec.yMax)/2,
          xFactor              = isNumber(spec.xFactor)?spec.xFactor:1,
          zFactor              = isNumber(spec.yFactor)?spec.yFactor:1,
          xShift               = isNumber(spec.xShift)?spec.xShift:0,
          zShift               = isNumber(spec.yShift)?spec.yShift:0,
          reverseShape         = isBoolean(spec.reverseShape)?spec.reverseShape:reverseShapes,
          reverseHole          = isBoolean(spec.reverseHole)?spec.reverseHole:reverseHoles,
          offX                 = xOffset-(balanced?centerX:0),
          offZ                 = zOffset-(balanced?centerZ:0),
          shapeCmdsLists       = isArray(spec.shapeCmds) ? spec.shapeCmds : [],
          holeCmdsListsArray   = isArray(spec.holeCmds) ? spec.holeCmds : [] , letterBox, letterOrigins;

      // ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~
      // Several scaling functions are created too, per warning
      var adjX                 = makeAdjust(letterScale,xFactor,offX,0,false,true),                     // no shift
          adjZ                 = makeAdjust(letterScale,zFactor,offZ,0,false,false),
          adjXfix              = makeAdjust(letterScale,xFactor,offX,xShift,false,true),                // shifted / fixed
          adjZfix              = makeAdjust(letterScale,zFactor,offZ,zShift,false,false),
          adjXrel              = makeAdjust(letterScale,xFactor,offX,xShift,true,true),                 // shifted / relative
          adjZrel              = makeAdjust(letterScale,zFactor,offZ,zShift,true,false),
          thisX, lastX, thisZ, lastZ, minX=NaN, maxX=NaN, minZ=NaN, maxZ=NaN, minXadj=NaN, maxXadj=NaN, minZadj=NaN, maxZadj=NaN;

      letterBox                = [ adjX(spec.xMin), adjX(spec.xMax), adjZ(spec.yMin), adjZ(spec.yMax) ];
      letterOrigins            = [ round(letterOffsetX), -1*adjX(0), -1*adjZ(0) ];

      // ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~
      // Scope warning:  letterOffsetX belongs to an outer closure
      // and persists through multiple characters
      letterOffsetX            = letterOffsetX+spec.wdth*letterScale;

      if(debug&&spec.show){
        console.log([minX,maxX,minZ,maxZ]);
        console.log([minXadj,maxXadj,minZadj,maxZadj])
      }

      return [ shapeCmdsLists.map(makeCmdsToMesh(reverseShape)) , holeCmdsListsArray.map(meshesFromCmdsListArray) , letterBox , letterOrigins ] ;

      function meshesFromCmdsListArray(cmdsListArray){
        return cmdsListArray.map(makeCmdsToMesh(reverseHole))
      };
      function makeCmdsToMesh(reverse){
        return function cmdsToMesh(cmdsList){
          var cmd              = getCmd(cmdsList,0),
              path             = new B.Path2(adjXfix(cmd[0]), adjZfix(cmd[1])), array, meshBuilder, j, last, first = 0;

          // ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~
          // Array length is used to determine curve type in the 'TheLeftover Font Format'  (TLFF)
          // 
          // IDIOCYNCRACY:  Odd-length arrays indicate relative coordinates; the first digit is discarded
          
          for ( j=1 ; j<cmdsList.length ; j++ ) {
            cmd                = getCmd(cmdsList,j);

            // ~  ~  ~  ~  ~  ~  ~  ~
            // Line
            if(cmd.length===2){
              path.addLineTo(adjXfix(cmd[0]),adjZfix(cmd[1])) 
            }
            if(cmd.length===3){
              path.addLineTo(adjXrel(cmd[1]),adjZrel(cmd[2]));
            }

            // ~  ~  ~  ~  ~  ~  ~  ~
            // Quadratic curve
            if(cmd.length===4){
              path.addQuadraticCurveTo(adjXfix(cmd[0]),adjZfix(cmd[1]),adjXfix(cmd[2]),adjZfix(cmd[3]))
            }
            if(cmd.length===5){
              path.addQuadraticCurveTo(adjXrel(cmd[1]),adjZrel(cmd[2]),adjXrel(cmd[3]),adjZrel(cmd[4]));
            }

            // ~  ~  ~  ~  ~  ~  ~  ~
            // Cubic curve
            if(cmd.length===6){
              path.addCubicCurveTo(adjXfix(cmd[0]),adjZfix(cmd[1]),adjXfix(cmd[2]),adjZfix(cmd[3]),adjXfix(cmd[4]),adjZfix(cmd[5]))
            }
            if(cmd.length===7){
              path.addCubicCurveTo(adjXrel(cmd[1]),adjZrel(cmd[2]),adjXrel(cmd[3]),adjZrel(cmd[4]),adjXrel(cmd[5]),adjZrel(cmd[6]))
            }
          }
          // Having created a Path2 instance with BABYLON utilities,
          // we turn it into an array and discard it
          array                = path.getPoints().map(point2Vector);

          // Sometimes redundant coordinates will cause artifacts - delete them!
          last                 = array.length - 1 ;
          if ( array[first].x===array[last].x && array[first].y===array[last].y ) { array = array.slice(1) }
          if ( reverse ) { array.reverse() }

          meshBuilder          = new B.PolygonMeshBuilder("MeshWriter-"+letter+index+"-"+weeid(), array, scene, earcut);
          return meshBuilder.build(true,thickness)
        }
      };
      function getCmd(list,ix){
        var cmd,len;
        lastX                  = thisX;
        lastZ                  = thisZ;
        cmd                    = list[ix];
        len                    = cmd.length;
        thisX                  = isRelativeLength(len) ? round((cmd[len-2]*xFactor)+thisX) : round(cmd[len-2]*xFactor);
        thisZ                  = isRelativeLength(len) ? round((cmd[len-1]*zFactor)+thisZ) : round(cmd[len-1]*zFactor);
        minX                   = thisX>minX?minX:thisX;
        maxX                   = thisX<maxX?maxX:thisX;
        minXadj                = thisX+xShift>minXadj?minXadj:thisX+xShift;
        maxXadj                = thisX+xShift<maxXadj?maxXadj:thisX+xShift;
        minZ                   = thisZ>minZ?minZ:thisZ;
        maxZ                   = thisZ<maxZ?maxZ:thisZ;
        minZadj                = thisZ+zShift>minZadj?minZadj:thisZ+zShift;
        maxZadj                = thisZ+zShift<maxZadj?maxZadj:thisZ+zShift;
        return cmd
      };

      // ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~
      // Returns the a scaling function, based on incoming parameters
      function makeAdjust(letterScale,factor,off,shift,relative,xAxis){
        if(relative){
          if(xAxis){
            return val => round(letterScale*((val*factor)+shift+lastX+off))
          }else{
            return val => round(letterScale*((val*factor)+shift+lastZ+off))
          }
        }else{
          return val => round(letterScale*((val*factor)+shift+off))
        }
      }
    };

    // ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~
    function punchHolesInShapes(shapesList,holesList){
      var letterMeshes         = [],j;
      for ( j=0 ; j<shapesList.length ; j++ ) {
        let shape              = shapesList[j];
        let holes              = holesList[j];
        if(isArray(holes)&&holes.length){
          letterMeshes.push ( punchHolesInShape(shape,holes,letter,i) )
        }else{
          letterMeshes.push ( shape )
        }
      }
      return letterMeshes
    };
    function punchHolesInShape(shape,holes,letter,i){
      var csgShape             = B.CSG.FromMesh(shape),k;
      for ( k=0; k<holes.length ; k++ ) {
        csgShape               = csgShape.subtract(B.CSG.FromMesh(holes[k]))
      }
      holes.forEach(h=>h.dispose());
      shape.dispose();
      return csgShape.toMesh("Net-"+letter+i+"-"+weeid(), null, scene)
    };
  };

  function makeMaterial(scene,letters,emissive,ambient,specular,diffuse,opac){
    var cm0                    = new B.StandardMaterial("mw-matl-"+letters+"-"+weeid(), scene);
    cm0.diffuseColor           = rgb2Bcolor3(diffuse);
    cm0.specularColor          = rgb2Bcolor3(specular);
    cm0.ambientColor           = rgb2Bcolor3(ambient);
    cm0.emissiveColor          = rgb2Bcolor3(emissive);
    cm0.alpha                  = opac;
    return cm0
  };

  // *-*=*  *=*-* *-*=*  *=*-* *-*=*  *=*-* *-*=*  *=*-* *-*=*  *=*-* *-*=*  *=*-* *-*=*  *=*-*
  //     FONT COMPRESSING AND DECOMPRESSING     FONT COMPRESSING AND DECOMPRESSING 
  //
  // One can reduce file size by 50% with a content-specific compression of font strings
  // Each letter object potentially has two long values, "shapeCmds" and "holeCmds"
  // These may be optionally compressed during construction of the file
  // The compressed versions are placed in "sC" and "hC"
  // The *first* time a letter is used, if it was compressed, it is decompressed
  function makeLetterSpec(fontSpec,letter){
    var letterSpec             = fontSpec[letter],
        singleMap              = cmds      => decodeList(cmds),
        doubleMap              = cmdslists => isArray(cmdslists)?cmdslists.map(singleMap):cmdslists;

    if(isObject(letterSpec)){
      if(!isArray(letterSpec.shapeCmds)&&isArray(letterSpec.sC)){
        letterSpec.shapeCmds   = letterSpec.sC.map(singleMap)
        letterSpec.sC          = null;
      }
      if(!isArray(letterSpec.holeCmds)&&isArray(letterSpec.hC)){
        letterSpec.holeCmds    = letterSpec.hC.map(doubleMap);
        letterSpec.hC          = null;
      }
    }
    return letterSpec;
  };

  function decodeList(str){
    var split    = str.split(" "),
        list     = [];
    split.forEach(function(cmds){
      if(cmds.length===12){list.push(decode6(cmds))}
      if(cmds.length===8) {list.push(decode4(cmds))}
      if(cmds.length===4) {list.push(decode2(cmds))}
    });
    return list
    function decode6(s){return [decode1(s,0,2),decode1(s,2,4),decode1(s,4,6),decode1(s,6,8),decode1(s,8,10),decode1(s,10,12)]};
    function decode4(s){return [decode1(s,0,2),decode1(s,2,4),decode1(s,4,6),decode1(s,6,8)]};
    function decode2(s){return [decode1(s,0,2),decode1(s,2,4)]};
    function decode1(s,start,end){return (frB128(s.substring(start,end))-4000)/2};
  };
  function codeList(list){
    var str   = "",
        xtra  = "";
    if(isArray(list)){
      list.forEach(function(cmds){
        if(cmds.length===6){str+=xtra+code6(cmds);xtra=" "}
        if(cmds.length===4){str+=xtra+code4(cmds);xtra=" "}
        if(cmds.length===2){str+=xtra+code2(cmds);xtra=" "}
      });
    }
    return str;

    function code6(a){return code1(a[0])+code1(a[1])+code1(a[2])+code1(a[3])+code1(a[4])+code1(a[5])};
    function code4(a){return code1(a[0])+code1(a[1])+code1(a[2])+code1(a[3])};
    function code2(a){return code1(a[0])+code1(a[1])};
    function code1(n){return toB128((n+n)+4000)};
  };

  function frB128(s){
    var result=0,i=-1,l=s.length-1;
    while(i++<l){result = result*128+b128back[s.charCodeAt(i)]}
    return result;
  };
  function toB128(i){
    var s                      = b128digits[(i%128)];
    i                          = Γ(i/128);
    while (i>0) {
      s                        = b128digits[(i%128)]+s;
      i                        = Γ(i/128);
    }
    return s;
  };
  function prepArray(){
    var pntr                   = -1,n;
    b128back                   = new Uint8Array(256);
    b128digits                 = new Array(128);
    while(160>pntr++){
      if(pntr<128){
        n                      = fr128to256(pntr);
        b128digits[pntr]       = String.fromCharCode(n);
        b128back[n]            = pntr
      }else{
        if(pntr===128){
          b128back[32]         = pntr
        }else{
          b128back[pntr+71]    = pntr
        }
      }
    };
    function fr128to256(n){if(n<92){return n<58?n<6?n+33:n+34:n+35}else{return n+69}}
  };
  //     FONT COMPRESSING AND DECOMPRESSING     FONT COMPRESSING AND DECOMPRESSING 
  // *-*=*  *=*-* *-*=*  *=*-* *-*=*  *=*-* *-*=*  *=*-* *-*=*  *=*-* *-*=*  *=*-* *-*=*  *=*-*

  // *-*=*  *=*-* *-*=*  *=*-* *-*=*  *=*-* *-*=*  *=*-* *-*=*  *=*-* *-*=*  *=*-* *-*=*  *=*-*
  //     PARAMETER QUALIFYING AND DEFAULTING     PARAMETER QUALIFYING AND DEFAULTING 
  // 
  // Screening and defaulting functions for incoming parameters
  function makePreferences(args){
    var prefs                  = {},p;
    if(isObject(p=args[1])){
      if(p["default-font"]){
        prefs.defaultFont      = p["default-font"]
      }else{
        if(p.defaultFont){
          prefs.defaultFont    = p.defaultFont
        }
      }
      if(p["mesh-origin"]){
        prefs.meshOrigin       = p["mesh-origin"]
      }else{
        if(p.meshOrigin){
          prefs.meshOrigin     = p.meshOrigin
        }
      }
      if(p.scale){
        prefs.scale            = p.scale
      }
      if(isBoolean(p.debug)){
        prefs.debug            = p.debug
      }
      cacheMethods(p.methods);
      return prefs
    }else{
      return { defaultFont: args[2] , scale: args[1] , debug: false }
    }
  }
  function cacheMethods(src){
    var incomplete             = false;
    if(isObject(src)){
      methodsList.forEach(function(meth){
        if(isObject(src[meth])){
          B[meth]              = src[meth]
        }else{
          incomplete           = meth
        }
      });
      if(!incomplete){supplementCurveFunctions()}
    }
    if(isString(incomplete)){
      throw new Error("Missing method '"+incomplete+"'")
    }
  }

  // ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~
  // Needed for making font curves
  // Thanks Gijs, wherever you are
  // 
  function supplementCurveFunctions(){
    if ( isObject ( B.Path2 ) ) {
      if ( !B.Path2.prototype.addQuadraticCurveTo ) {
        B.Path2.prototype.addQuadraticCurveTo = function(redX, redY, blueX, blueY){
          var points           = this.getPoints();
          var lastPoint        = points[points.length - 1];
          var origin           = new B.Vector3(lastPoint.x, lastPoint.y, 0);
          var control          = new B.Vector3(redX, redY, 0);
          var destination      = new B.Vector3(blueX, blueY, 0);
          var nb_of_points     = curveSampleSize;
          var curve            = B.Curve3.CreateQuadraticBezier(origin, control, destination, nb_of_points);
          var curvePoints      = curve.getPoints();
          for(var i=1; i<curvePoints.length; i++){
            this.addLineTo(curvePoints[i].x, curvePoints[i].y);
          }
        }
      }
      if ( !B.Path2.prototype.addCubicCurveTo ) {
        B.Path2.prototype.addCubicCurveTo = function(redX, redY, greenX, greenY, blueX, blueY){
          var points           = this.getPoints();
          var lastPoint        = points[points.length - 1];
          var origin           = new B.Vector3(lastPoint.x, lastPoint.y, 0);
          var control1         = new B.Vector3(redX, redY, 0);
          var control2         = new B.Vector3(greenX, greenY, 0);
          var destination      = new B.Vector3(blueX, blueY, 0);
          var nb_of_points     = Math.floor(0.3+curveSampleSize*1.5);
          var curve            = B.Curve3.CreateCubicBezier(origin, control1, control2, destination, nb_of_points);
          var curvePoints      = curve.getPoints();
          for(var i=1; i<curvePoints.length; i++){
            this.addLineTo(curvePoints[i].x, curvePoints[i].y);
          }
        }
      }
    }
  }
  //  ~  -  =  ~  -  =  ~  -  =  ~  -  =  ~  -  =  
  // Applies a test to potential incoming parameters
  // If the test passes, the parameters are used, otherwise the default is used
  function setOption(opts, field, tst, defalt) { return tst(opts[field]) ? opts[field] : defalt };

  //     PARAMETER QUALIFYING AND DEFAULTING     PARAMETER QUALIFYING AND DEFAULTING 
  // *-*=*  *=*-* *-*=*  *=*-* *-*=*  *=*-* *-*=*  *=*-* *-*=*  *=*-* *-*=*  *=*-* *-*=*  *=*-*

  // *-*=*  *=*-* *-*=*  *=*-* *-*=*  *=*-* *-*=*  *=*-* *-*=*  *=*-* *-*=*  *=*-* *-*=*  *=*-*
  // Conversion functions
  function rgb2Bcolor3(rgb){
    rgb                        = rgb.replace("#","");
    return new B.Color3(convert(rgb.substring(0,2)),convert(rgb.substring(2,4)),convert(rgb.substring(4,6)));
    function convert(x){return Γ(1000*Math.max(0,Math.min((isNumber(parseInt(x,16))?parseInt(x,16):0)/255,1)))/1000}
  };
  function point2Vector(point){
    return new B.Vector2(round(point.x),round(point.y))
  };
  function merge(arrayOfMeshes){
    return arrayOfMeshes.length===1 ? arrayOfMeshes[0] : B.Mesh.MergeMeshes(arrayOfMeshes, true)
  };

  // *-*=*  *=*-* *-*=*  *=*-* *-*=*  *=*-* *-*=*  *=*-* *-*=*  *=*-* *-*=*  *=*-* *-*=*  *=*-*
  // Boolean test functions
  function isPositiveNumber(mn) { return typeof mn === "number" && !isNaN(mn) ? 0 < mn : false } ;
  function isNumber(mn)         { return typeof mn === "number" } ;
  function isBoolean(mn)        { return typeof mn === "boolean" } ;
  function isAmplitude(ma)      { return typeof ma === "number" && !isNaN(ma) ? 0 <= ma && ma <= 1 : false } ;
  function isObject(mo)         { return mo != null && typeof mo === "object" || typeof mo === "function" } ;
  function isArray(ma)          { return ma != null && typeof ma === "object" && ma.constructor === Array } ; 
  function isString(ms)         { return typeof ms === "string" ? ms.length>0 : false }  ;
  function isSupportedFont(ff)  { return isObject(FONTS[ff]) } ;
  function isSupportedAnchor(a) { return a==="left"||a==="right"||a==="center" } ;
  function isRelativeLength(l)  { return l===3||l===5||l===7 } ;
  function weeid()              { return Math.floor(Math.random()*1000000) } ;
  function round(n)             { return Γ(0.3+n*1000000)/1000000 }
}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
      __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(1)))

/***/ }),
/* 1 */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
return this;
})();

try {
// This works if eval is allowed (see CSP)
g = g || new Function("return this")();
} catch (e) {
// This works if the window reference is available
if (typeof window === "object") g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;//  RUBIK MONO ONE  RUBIK MONO ONE  RUBIK MONO ONE
// 

!(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_RESULT__ = (function(){

  return function(codeList){

    var font={reverseHoles:false,reverseShapes:true},nbsp=' ';

    font["a"]        = {
      sC             : [
                         'D½AB AÃAB A±ABA¤AO AuA]AuAi AuAuAwA{ E*Ky E<L:E§L: J0L: JyL:J­Ky N>A{ N@AuN@Ai N@A]N3AO N%ABM·AB J½AB JZABJ@A© I±Bµ F%Bµ EuA© EZABD½AB'
                       ],
      hC             : [
                         ['F¯E» I(E» G½I8 F¯E»']
                       ],
      xMin           : 25,
      xMax           : 831,
      yMin           : 0,
      yMax           : 700,
      wdth           : 850
    };
    font["b"]        = {
      sC             : [
                         'M±D{ M±ABIZAB C!AB B¯ABB}AR BmAcBmAy BmK§ BmK½B}L* B¯L:C!L: IFL: KcL:LjKS MqJmMqH¿ MqH¹ MqH6M<Gs L«G.LPF¿ LÁF§MVF3 M±EcM±D{'
                       ],
      hC             : [
                         ['I=HI IRHgIRH­ IRI.I=IH I(IcH£Ic FuIc FuH, H£H, I(H,I=HI','H¹E_ FuE_ FuC½ H¹C½ IDC½I[D9 IsDXIsD¡ IsD£ IsE%IYEB I@E_H¹E_']
                       ],
      xMin           : 85,
      xMax           : 806,
      yMin           : 0,
      yMax           : 700,
      wdth           : 850
    };
    font["c"]        = {
      sC             : [
                         'F]GÁ F]E_ F]D¯F¶Da GJD4H"D4 H}D4I#DP ILDmIhE! I¥EXJ.EX M_EX MqEXM~EK M­E>M­E, M­CsLZBX K¥A¯JqA^ I_A.G»A. FRA.E/Al C¯B(C%C3 B@D>B@E« B@Gu B@I>C%JJ DgLNH(LN I_LNJqKÂ K¥KqLZK# M­I­M­HP M­H>M~H1 MqH#M_H# J.H# I¥H#IhHZ ILH³I#I, H}IHH"IH GJIHF¶H¿ F]HqF]GÁ'
                       ],
      xMin           : 63,
      xMax           : 804,
      yMin           : -10,
      yMax           : 710,
      wdth           : 850
    };
    font["d"]        = {
      sC             : [
                         'B³L: G±L: IkL:J¯K£ L.KFLµJ< MwI2MwGe MwE» MwCXKÁBF JXABG±AB B³AB B{ABBkAR BZAcBZAy BZK§ BZK½BkL* B{L:B³L:'
                       ],
      hC             : [
                         ['G»I4 FcI4 FcDH G»DH HwDHI-Dv IeE!IeEu IeG« IeHZI-Hª HwI4G»I4']
                       ],
      xMin           : 76,
      xMax           : 794,
      yMin           : 0,
      yMax           : 700,
      wdth           : 850
    };
    font["e"]        = {
      sC             : [
                         'F¡EF F¡DH L¿DH M2DHMBD8 MRD(MRCµ MRAy MRAcMBAR M2ABL¿AB C,AB B¹ABB©AR BwAcBwAy BwK§ BwK½B©L* B¹L:C,L: L«L: LÁL:M.L* M>K½M>K§ M>Ik M>ITM.ID LÁI4L«I4 F¡I4 F¡H6 K½H6 L0H6L@H% LPG¹LPG£ LPE} LPEgL@EV L0EFK½EF F¡EF'
                       ],
      xMin           : 90,
      xMax           : 776,
      yMin           : 0,
      yMax           : 700,
      wdth           : 850
    };
    font["f"]        = {
      sC             : [
                         'C@L: M8L: MNL:M_L* MoK½MoK§ MoIT MoI>M_I. MNHÁM8HÁ G%HÁ G%GÁ L6GÁ LLGÁL]G± LmG¡LmGi LmE: LmE#L]D· LLD§L6D§ G%D§ G%Ay G%AcF¹AR F©ABFqAB C@AB C*ABB½AR B­AcB­Ay B­K§ B­K½B½L* C*L:C@L:'
                       ],
      xMin           : 100,
      xMax           : 790,
      yMin           : 0,
      yMax           : 700,
      wdth           : 850
    };
    font["g"]        = {
      sC             : [
                         'HÃG± MeG± M{G±M­G¡ M½GoM½GX M½E§ M½CTLKBA J}A.H(A. ETA.C¨BA B6CTB6E« B6Gu B6H±BrI§ C,J{C¹KB EgLNH(LN IHLNJML) KRK§KÄKM LqJ¹M(JV M­IgM­H³ M­H¡M~Hr MqHeM_He I½He IsHeIaH{ I!IHH7IH GLIHF²HÁ FRHuFRH2 FRE_ FRD±F±Db GJD4H7D4 I#D4I`D` I½D­I¿ED HÃED H­EDH{ET HkEeHkE{ HkGX HkGoH{G¡ H­G±HÃG±'
                       ],
      xMin           : 58,
      xMax           : 812,
      yMin           : -10,
      yMax           : 710,
      wdth           : 850
    };
    font["h"]        = {
      sC             : [
                         'IµL: M.L: MDL:MTL* MeK½MeK§ MeAy MeAcMTAR MDABM.AB IµAB I}ABImAR I]AcI]Ay I]E! FLE! FLAy FLAcF<AR F,ABE¹AB B{AB BeABBTAQ BDAaBDAy BDK§ BDK½BTL* BeL:B{L: E¹L: F,L:F<L* FLK½FLK§ FLHk I]Hk I]K§ I]K½ImL* I}L:IµL:'
                       ],
      xMin           : 65,
      xMax           : 785,
      yMin           : 0,
      yMax           : 700,
      wdth           : 850
    };
    font["i"]        = {
      sC             : [
                         'L±HÃ J#HÃ J#D] L±D] M#D]M4DL MDD<MDD% MDAy MDAcM4AR M#ABL±AB B½AB B§ABBuAR BeAcBeAy BeD% BeD<BuDL B§D]B½D] E«D] E«HÃ B½HÃ B§HÃBuI0 BeI@BeIV BeK§ BeK½BuL* B§L:B½L: L±L: M#L:M4L* MDK½MDK§ MDIV MDI@M4I0 M#HÃL±HÃ'
                       ],
      xMin           : 81,
      xMax           : 769,
      yMin           : 0,
      yMax           : 700,
      wdth           : 850
    };
    font["j"]        = {
      sC             : [
                         'MBK§ MBE« MBCXK{BC J2A.GeA. F.A.D½A^ C©A¯C.BX AyCwAyE, AyE>A¨EK AµEXB#EX EiEX E¯EXE½E, F@D4GZD4 H8D4HpDa I%D¯I%E_ I%HÃ CLHÃ C6HÃC%I0 B¹I@B¹IV B¹K§ B¹K½C%L* C6L:CLL: L¯L: M!L:M2L* MBK½MBK§'
                       ],
      xMin           : 27,
      xMax           : 768,
      yMin           : -10,
      yMax           : 700,
      wdth           : 850
    };
    font["k"]        = {
      sC             : [
                         'J2G0 NNA« NVA}NVAm NVA]NIAO N<ABN*AB J%AB IyABI_Ac F¥D³ F¥Ay F¥AcFsAR FcABFLAB C0AB B½ABB­AR B{AcB{Ay B{K§ B{K½B­L* B½L:C0L: FLL: FcL:FsL* F¥K½F¥K§ F¥H¯ I>K» I]L:I«L: MkL: M}L:M¬L- M¹KÃM¹K² M¹K¡M¯Ks J2G0'
                       ],
      xMin           : 92,
      xMax           : 842,
      yMin           : 0,
      yMax           : 700,
      wdth           : 850
    };
    font["l"]        = {
      sC             : [
                         'GwK§ GwD] LÃD] M6D]MFDL MVD<MVD% MVAy MVAcMFAR M6ABLÃAB C³AB C{ABCkAR CZAcCZAy CZK§ CZK½CkL* C{L:C³L: G@L: GVL:GgL* GwK½GwK§'
                       ],
      xMin           : 140,
      xMax           : 778,
      yMin           : 0,
      yMax           : 700,
      wdth           : 850
    };
    font["m"]        = {
      sC             : [
                         'JqL: M_L: MuL:M§L* M·K½M·K§ M·Ay M·AcM§AR MuABM_AB JuAB J_ABJNAR J>AcJ>Ay J>Fe I,DD HÃD,H«CÂ HqCµHXCµ GPCµ G.CµF»D# F¥D6F}DD EkFe EkAy EkAcEZAR EJABE4AB BJAB B4ABB#AR A·AcA·Ay A·K§ A·K½B#L* B4L:BJL: E8L: EqL:E±K§ G·H# I½K§ J8L:JqL:'
                       ],
      xMin           : 41,
      xMax           : 809,
      yMin           : 0,
      yMax           : 700,
      wdth           : 850
    };
    font["n"]        = {
      sC             : [
                         'IµL: L©L: L¿L:M,L* M<K½M<K§ M<Ay M<AcM,AR L¿ABL©AB JDAB I·ABIuAm F@E» F@Ay F@AcF0AR EÃABE­AB B¹AB B£ABBqAR BaAcBaAy BaK§ BaK½BqL* B£L:B¹L: EZL: E­L:F(Kµ I]G8 I]K§ I]K½ImL* I}L:IµL:'
                       ],
      xMin           : 79,
      xMax           : 765,
      yMin           : 0,
      yMax           : 700,
      wdth           : 850
    };
    font["o"]        = {
      sC             : [
                         'M}Gy M}E§ M}D6L»C, L4B!JµAi IqA.G·A. F8A.D¹Ai CuB!B³C, B,D6B,E« B,Gu B,J!C|K8 EJLNG·LN J_LNL-K8 M}J!M}Gy'
                       ],
      hC             : [
                         ['FHGÁ FHE_ FHD¯F¢Da G6D4G·D4 HsD4I)Db IaD±IaEa IaG¿ IaHoI)H¾ HsIHG·IH G6IHF¢H¿ FHHqFHGÁ']
                       ],
      xMin           : 53,
      xMax           : 797,
      yMin           : -10,
      yMax           : 710,
      wdth           : 850
    };
    font["p"]        = {
      sC             : [
                         'CuL: IaL: K}L:M!K0 NHJ%NHHN NHFwM<Ex L0DyIaDy GXDy GXAy GXAcGHAR G8ABG!AB CuAB C_ABCNAR C>AcC>Ay C>K§ C>K½CNL* C_L:CuL:'
                       ],
      hC             : [
                         ['IXIF GZIF GZGk IXGk I©GkJ"G© J@H!J@HT J@H©J#I% I«IFIXIF']
                       ],
      xMin           : 126,
      xMax           : 835,
      yMin           : 0,
      yMax           : 700,
      wdth           : 850
    };
    font["q"]        = {
      sC             : [
                         'M{Gy M{E§ M{C_L8BF ML@¡ MZ@cMK@Q M<@@M*@@ J,@@ Im@@IJ@s H¹A4 HVA.GµA. F6A.D·Ai CsB!B±C, B*D6B*E« B*Gu B*IFB±JP CsKZD·K· F6LNGµLN IoLNJ³K· L2KZL¹JP M{IFM{Gy'
                       ],
      hC             : [
                         ['FFGÁ FFE_ FFD¯F~Da G4D4GµD4 HqD4I&Db I_D±I_Ea I_G¿ I_HoI&H¾ HqIHGµIH G4IHF~H¿ FFHqFFGÁ']
                       ],
      xMin           : 52,
      xMax           : 796,
      yMin           : -65,
      yMax           : 710,
      wdth           : 850
    };
    font["r"]        = {
      sC             : [
                         'L8EL N.A£ N2AyN2Ak N2A]N$AO M»ABM©AB J@AB I§ABImAu H<D£ F·D£ F·Ay F·AcF§AR FuABF_AB C.AB B»ABB«AR ByAcByAy ByK§ ByK½B«L* B»L:C.L: I8L: KJL:LkK8 M­J6M­HV M­FFL8EL'
                       ],
      hC             : [
                         ['H±IF F·IF F·Gs H±Gs I:GsIRG² IkH,IkHX IkH§ISI$ I<IFH±IF']
                       ],
      xMin           : 91,
      xMax           : 824,
      yMin           : 0,
      yMax           : 700,
      wdth           : 850
    };
    font["s"]        = {
      sC             : [
                         'H¥H> KJG»LUG1 MaFJMaD} MaC.K¹B. JLA.G¡A. E0A.CxB) B>C#B>DX B>D§BmD§ E©D§ F0D§FJDk F¥D4G¯D4 IJD4IJDo IJDµH±E& HRE<FÃEP BkE·BkH¥ BkJPCÃKO EVLNGªLN J8LNKuKI M0JDM0I0 M0H¿M"H³ L¹H§L£H§ IRH§ I.H§H·H¿ HeIHG§IH F¡IHF¡H³ F¡HsG2Hb GgHPH¥H>'
                       ],
      xMin           : 62,
      xMax           : 783,
      yMin           : -10,
      yMax           : 710,
      wdth           : 850
    };
    font["t"]        = {
      sC             : [
                         'BcL: MFL: M]L:MmL* M}K½M}K§ M}IB M}I,MmH¿ M]H¯MFH¯ I»H¯ I»Ay I»AcI«AR IyABIcAB FFAB F0ABEÃAR E³AcE³Ay E³H¯ BcH¯ BLH¯B<H¿ B,I,B,IB B,K§ B,K½B<L* BLL:BcL:'
                       ],
      xMin           : 53,
      xMax           : 797,
      yMin           : 0,
      yMax           : 700,
      wdth           : 850
    };
    font["u"]        = {
      sC             : [
                         'L)B@ JeA.G·A. EDA.C¢B@ B:CRB:E« B:K§ B:K½BJL* BZL:BqL: EµL: F(L:F8L* FHK½FHK§ FHEi FHD¹F¢Dk G6D>G·D> HsD>I)Dk IaD¹IaEi IaK§ IaK½IqL* I£L:I¹L: M8L: MNL:M_L* MoK½MoK§ MoE« MoCRL)B@'
                       ],
      xMin           : 60,
      xMax           : 790,
      yMin           : -10,
      yMax           : 700,
      wdth           : 850
    };
    font["v"]        = {
      sC             : [
                         'JTL: M{L: M¯L:M¼L- N%KÃN%K· N%K«N#K£ JXA§ JBABI¡AB F,AB EkABETA¡ A§K£ A¥K«A¥K· A¥KÃA²L- A¿L:B.L: ETL: EyL:E²L$ F%K³F.Ky G·E{ I{Ky I¯L:JTL:'
                       ],
      xMin           : 32,
      xMax           : 818,
      yMin           : 0,
      yMax           : 700,
      wdth           : 850
    };
    font["w"]        = {
      sC             : [
                         'E¯AB CuAB CNABC5AY B¿AqB»A± AkK« AkK± AkKÃAxL- A§L:A¹L: DoL: D¹L:E/L! EHK­ELKg E¯F¥ F£I@ F©ITFÀIh G4I{GXI{ HaI{ H§I{H¾Ih I2ITI8I@ J,F¥ JmKg JqK­J¬L! K!L:KJL: N!L: N4L:NAL- NNKÃNNK± NNK« LÃA± L¿AqL¦AY LkABLDAB J,AB I«ABIrAT IZAgITAy G¿DÁ FeAy F_AgFGAT F0ABE¯AB'
                       ],
      xMin           : 20,
      xMax           : 838,
      yMin           : 0,
      yMax           : 700,
      wdth           : 850
    };
    font["x"]        = {
      sC             : [
                         'JNF± M{A§ M£A}M£Am M£A]MtAO MgABMTAB J%AB IqABITAq GµC½ FTAq F6ABE¥AB BRAB B@ABB3AO B%A]B%Am B%A}B,A§ EgG( B]Ky BVK£BVK³ BVKÃBdL- BqL:B¥L: F2L: FgL:F©K« H!I« IJK¯ ImL:I¿L: MJL: M]L:MjL- MwKÃMwK³ MwK£MqKy JNF±'
                       ],
      xMin           : 50,
      xMax           : 799,
      yMin           : 0,
      yMax           : 700,
      wdth           : 850
    };
    font["y"]        = {
      sC             : [
                         'JNL: MqL: M¥L:M²L- M¿KÃM¿Kµ M¿K§M¹Ky IÁE6 IÁAy IÁAcI±AR I¡ABIiAB F>AB F(ABE»AR E«AcE«Ay E«E6 AµKy A¯K§A¯Kµ A¯KÃA¼L- B%L:B8L: E_L: E¹L:F0K¯ GµH£ I{K« I»L:JNL:'
                       ],
      xMin           : 37,
      xMax           : 813,
      yMin           : 0,
      yMax           : 700,
      wdth           : 850
    };
    font["z"]        = {
      sC             : [
                         'G½D] L»D] M.D]M>DL MND<MND% MNAy MNAcM>AR M.ABL»AB B·AB B¡ABBoAR B_AcB_Ay B_C» B_DFB}D_ GuHÃ C>HÃ C(HÃB»I0 B«I@B«IV B«K§ B«K½B»L* C(L:C>L: LoL: L§L:L·L* M#K½M#K§ M#Ig M#I<L­I# G½D]'
                       ],
      xMin           : 78,
      xMax           : 774,
      yMin           : 0,
      yMax           : 700,
      wdth           : 850
    };
    font["A"]        = {
      sC             : [
                         'D½AB AÃAB A±ABA¤AO AuA]AuAi AuAuAwA{ E*Ky E<L:E§L: J0L: JyL:J­Ky N>A{ N@AuN@Ai N@A]N3AO N%ABM·AB J½AB JZABJ@A© I±Bµ F%Bµ EuA© EZABD½AB'
                       ],
      hC             : [
                         ['F¯E» I(E» G½I8 F¯E»']
                       ],
      xMin           : 25,
      xMax           : 831,
      yMin           : 0,
      yMax           : 700,
      wdth           : 850
    };
    font["B"]        = {
      sC             : [
                         'M±D{ M±ABIZAB C!AB B¯ABB}AR BmAcBmAy BmK§ BmK½B}L* B¯L:C!L: IFL: KcL:LjKS MqJmMqH¿ MqH¹ MqH6M<Gs L«G.LPF¿ LÁF§MVF3 M±EcM±D{'
                       ],
      hC             : [
                         ['I=HI IRHgIRH­ IRI.I=IH I(IcH£Ic FuIc FuH, H£H, I(H,I=HI','H¹E_ FuE_ FuC½ H¹C½ IDC½I[D9 IsDXIsD¡ IsD£ IsE%IYEB I@E_H¹E_']
                       ],
      xMin           : 85,
      xMax           : 806,
      yMin           : 0,
      yMax           : 700,
      wdth           : 850
    };
    font["C"]        = {
      sC             : [
                         'F]GÁ F]E_ F]D¯F¶Da GJD4H"D4 H}D4I#DP ILDmIhE! I¥EXJ.EX M_EX MqEXM~EK M­E>M­E, M­CsLZBX K¥A¯JqA^ I_A.G»A. FRA.E/Al C¯B(C%C3 B@D>B@E« B@Gu B@I>C%JJ DgLNH(LN I_LNJqKÂ K¥KqLZK# M­I­M­HP M­H>M~H1 MqH#M_H# J.H# I¥H#IhHZ ILH³I#I, H}IHH"IH GJIHF¶H¿ F]HqF]GÁ'
                       ],
      xMin           : 63,
      xMax           : 804,
      yMin           : -10,
      yMax           : 710,
      wdth           : 850
    };
    font["D"]        = {
      sC             : [
                         'B³L: G±L: IkL:J¯K£ L.KFLµJ< MwI2MwGe MwE» MwCXKÁBF JXABG±AB B³AB B{ABBkAR BZAcBZAy BZK§ BZK½BkL* B{L:B³L:'
                       ],
      hC             : [
                         ['G»I4 FcI4 FcDH G»DH HwDHI-Dv IeE!IeEu IeG« IeHZI-Hª HwI4G»I4']
                       ],
      xMin           : 76,
      xMax           : 794,
      yMin           : 0,
      yMax           : 700,
      wdth           : 850
    };
    font["E"]        = {
      sC             : [
                         'F¡EF F¡DH L¿DH M2DHMBD8 MRD(MRCµ MRAy MRAcMBAR M2ABL¿AB C,AB B¹ABB©AR BwAcBwAy BwK§ BwK½B©L* B¹L:C,L: L«L: LÁL:M.L* M>K½M>K§ M>Ik M>ITM.ID LÁI4L«I4 F¡I4 F¡H6 K½H6 L0H6L@H% LPG¹LPG£ LPE} LPEgL@EV L0EFK½EF F¡EF'
                       ],
      xMin           : 90,
      xMax           : 776,
      yMin           : 0,
      yMax           : 700,
      wdth           : 850
    };
    font["F"]        = {
      sC             : [
                         'C@L: M8L: MNL:M_L* MoK½MoK§ MoIT MoI>M_I. MNHÁM8HÁ G%HÁ G%GÁ L6GÁ LLGÁL]G± LmG¡LmGi LmE: LmE#L]D· LLD§L6D§ G%D§ G%Ay G%AcF¹AR F©ABFqAB C@AB C*ABB½AR B­AcB­Ay B­K§ B­K½B½L* C*L:C@L:'
                       ],
      xMin           : 100,
      xMax           : 790,
      yMin           : 0,
      yMax           : 700,
      wdth           : 850
    };
    font["G"]        = {
      sC             : [
                         'HÃG± MeG± M{G±M­G¡ M½GoM½GX M½E§ M½CTLKBA J}A.H(A. ETA.C¨BA B6CTB6E« B6Gu B6H±BrI§ C,J{C¹KB EgLNH(LN IHLNJML) KRK§KÄKM LqJ¹M(JV M­IgM­H³ M­H¡M~Hr MqHeM_He I½He IsHeIaH{ I!IHH7IH GLIHF²HÁ FRHuFRH2 FRE_ FRD±F±Db GJD4H7D4 I#D4I`D` I½D­I¿ED HÃED H­EDH{ET HkEeHkE{ HkGX HkGoH{G¡ H­G±HÃG±'
                       ],
      xMin           : 58,
      xMax           : 812,
      yMin           : -10,
      yMax           : 710,
      wdth           : 850
    };
    font["H"]        = {
      sC             : [
                         'IµL: M.L: MDL:MTL* MeK½MeK§ MeAy MeAcMTAR MDABM.AB IµAB I}ABImAR I]AcI]Ay I]E! FLE! FLAy FLAcF<AR F,ABE¹AB B{AB BeABBTAQ BDAaBDAy BDK§ BDK½BTL* BeL:B{L: E¹L: F,L:F<L* FLK½FLK§ FLHk I]Hk I]K§ I]K½ImL* I}L:IµL:'
                       ],
      xMin           : 65,
      xMax           : 785,
      yMin           : 0,
      yMax           : 700,
      wdth           : 850
    };
    font["I"]        = {
      sC             : [
                         'L±HÃ J#HÃ J#D] L±D] M#D]M4DL MDD<MDD% MDAy MDAcM4AR M#ABL±AB B½AB B§ABBuAR BeAcBeAy BeD% BeD<BuDL B§D]B½D] E«D] E«HÃ B½HÃ B§HÃBuI0 BeI@BeIV BeK§ BeK½BuL* B§L:B½L: L±L: M#L:M4L* MDK½MDK§ MDIV MDI@M4I0 M#HÃL±HÃ'
                       ],
      xMin           : 81,
      xMax           : 769,
      yMin           : 0,
      yMax           : 700,
      wdth           : 850
    };
    font["J"]        = {
      sC             : [
                         'MBK§ MBE« MBCXK{BC J2A.GeA. F.A.D½A^ C©A¯C.BX AyCwAyE, AyE>A¨EK AµEXB#EX EiEX E¯EXE½E, F@D4GZD4 H8D4HpDa I%D¯I%E_ I%HÃ CLHÃ C6HÃC%I0 B¹I@B¹IV B¹K§ B¹K½C%L* C6L:CLL: L¯L: M!L:M2L* MBK½MBK§'
                       ],
      xMin           : 27,
      xMax           : 768,
      yMin           : -10,
      yMax           : 700,
      wdth           : 850
    };
    font["K"]        = {
      sC             : [
                         'J2G0 NNA« NVA}NVAm NVA]NIAO N<ABN*AB J%AB IyABI_Ac F¥D³ F¥Ay F¥AcFsAR FcABFLAB C0AB B½ABB­AR B{AcB{Ay B{K§ B{K½B­L* B½L:C0L: FLL: FcL:FsL* F¥K½F¥K§ F¥H¯ I>K» I]L:I«L: MkL: M}L:M¬L- M¹KÃM¹K² M¹K¡M¯Ks J2G0'
                       ],
      xMin           : 92,
      xMax           : 842,
      yMin           : 0,
      yMax           : 700,
      wdth           : 850
    };
    font["L"]        = {
      sC             : [
                         'GwK§ GwD] LÃD] M6D]MFDL MVD<MVD% MVAy MVAcMFAR M6ABLÃAB C³AB C{ABCkAR CZAcCZAy CZK§ CZK½CkL* C{L:C³L: G@L: GVL:GgL* GwK½GwK§'
                       ],
      xMin           : 140,
      xMax           : 778,
      yMin           : 0,
      yMax           : 700,
      wdth           : 850
    };
    font["M"]        = {
      sC             : [
                         'JqL: M_L: MuL:M§L* M·K½M·K§ M·Ay M·AcM§AR MuABM_AB JuAB J_ABJNAR J>AcJ>Ay J>Fe I,DD HÃD,H«CÂ HqCµHXCµ GPCµ G.CµF»D# F¥D6F}DD EkFe EkAy EkAcEZAR EJABE4AB BJAB B4ABB#AR A·AcA·Ay A·K§ A·K½B#L* B4L:BJL: E8L: EqL:E±K§ G·H# I½K§ J8L:JqL:'
                       ],
      xMin           : 41,
      xMax           : 809,
      yMin           : 0,
      yMax           : 700,
      wdth           : 850
    };
    font["N"]        = {
      sC             : [
                         'IµL: L©L: L¿L:M,L* M<K½M<K§ M<Ay M<AcM,AR L¿ABL©AB JDAB I·ABIuAm F@E» F@Ay F@AcF0AR EÃABE­AB B¹AB B£ABBqAR BaAcBaAy BaK§ BaK½BqL* B£L:B¹L: EZL: E­L:F(Kµ I]G8 I]K§ I]K½ImL* I}L:IµL:'
                       ],
      xMin           : 79,
      xMax           : 765,
      yMin           : 0,
      yMax           : 700,
      wdth           : 850
    };
    font["O"]        = {
      sC             : [
                         'M}Gy M}E§ M}D6L»C, L4B!JµAi IqA.G·A. F8A.D¹Ai CuB!B³C, B,D6B,E« B,Gu B,J!C|K8 EJLNG·LN J_LNL-K8 M}J!M}Gy'
                       ],
      hC             : [
                         ['FHGÁ FHE_ FHD¯F¢Da G6D4G·D4 HsD4I)Db IaD±IaEa IaG¿ IaHoI)H¾ HsIHG·IH G6IHF¢H¿ FHHqFHGÁ']
                       ],
      xMin           : 53,
      xMax           : 797,
      yMin           : -10,
      yMax           : 710,
      wdth           : 850
    };
    font["P"]        = {
      sC             : [
                         'CuL: IaL: K}L:M!K0 NHJ%NHHN NHFwM<Ex L0DyIaDy GXDy GXAy GXAcGHAR G8ABG!AB CuAB C_ABCNAR C>AcC>Ay C>K§ C>K½CNL* C_L:CuL:'
                       ],
      hC             : [
                         ['IXIF GZIF GZGk IXGk I©GkJ"G© J@H!J@HT J@H©J#I% I«IFIXIF']
                       ],
      xMin           : 126,
      xMax           : 835,
      yMin           : 0,
      yMax           : 700,
      wdth           : 850
    };
    font["Q"]        = {
      sC             : [
                         'M{Gy M{E§ M{C_L8BF ML@¡ MZ@cMK@Q M<@@M*@@ J,@@ Im@@IJ@s H¹A4 HVA.GµA. F6A.D·Ai CsB!B±C, B*D6B*E« B*Gu B*IFB±JP CsKZD·K· F6LNGµLN IoLNJ³K· L2KZL¹JP M{IFM{Gy'
                       ],
      hC             : [
                         ['FFGÁ FFE_ FFD¯F~Da G4D4GµD4 HqD4I&Db I_D±I_Ea I_G¿ I_HoI&H¾ HqIHGµIH G4IHF~H¿ FFHqFFGÁ']
                       ],
      xMin           : 52,
      xMax           : 796,
      yMin           : -65,
      yMax           : 710,
      wdth           : 850
    };
    font["R"]        = {
      sC             : [
                         'L8EL N.A£ N2AyN2Ak N2A]N$AO M»ABM©AB J@AB I§ABImAu H<D£ F·D£ F·Ay F·AcF§AR FuABF_AB C.AB B»ABB«AR ByAcByAy ByK§ ByK½B«L* B»L:C.L: I8L: KJL:LkK8 M­J6M­HV M­FFL8EL'
                       ],
      hC             : [
                         ['H±IF F·IF F·Gs H±Gs I:GsIRG² IkH,IkHX IkH§ISI$ I<IFH±IF']
                       ],
      xMin           : 91,
      xMax           : 824,
      yMin           : 0,
      yMax           : 700,
      wdth           : 850
    };
    font["S"]        = {
      sC             : [
                         'H¥H> KJG»LUG1 MaFJMaD} MaC.K¹B. JLA.G¡A. E0A.CxB) B>C#B>DX B>D§BmD§ E©D§ F0D§FJDk F¥D4G¯D4 IJD4IJDo IJDµH±E& HRE<FÃEP BkE·BkH¥ BkJPCÃKO EVLNGªLN J8LNKuKI M0JDM0I0 M0H¿M"H³ L¹H§L£H§ IRH§ I.H§H·H¿ HeIHG§IH F¡IHF¡H³ F¡HsG2Hb GgHPH¥H>'
                       ],
      xMin           : 62,
      xMax           : 783,
      yMin           : -10,
      yMax           : 710,
      wdth           : 850
    };
    font["T"]        = {
      sC             : [
                         'BcL: MFL: M]L:MmL* M}K½M}K§ M}IB M}I,MmH¿ M]H¯MFH¯ I»H¯ I»Ay I»AcI«AR IyABIcAB FFAB F0ABEÃAR E³AcE³Ay E³H¯ BcH¯ BLH¯B<H¿ B,I,B,IB B,K§ B,K½B<L* BLL:BcL:'
                       ],
      xMin           : 53,
      xMax           : 797,
      yMin           : 0,
      yMax           : 700,
      wdth           : 850
    };
    font["U"]        = {
      sC             : [
                         'L)B@ JeA.G·A. EDA.C¢B@ B:CRB:E« B:K§ B:K½BJL* BZL:BqL: EµL: F(L:F8L* FHK½FHK§ FHEi FHD¹F¢Dk G6D>G·D> HsD>I)Dk IaD¹IaEi IaK§ IaK½IqL* I£L:I¹L: M8L: MNL:M_L* MoK½MoK§ MoE« MoCRL)B@'
                       ],
      xMin           : 60,
      xMax           : 790,
      yMin           : -10,
      yMax           : 700,
      wdth           : 850
    };
    font["V"]        = {
      sC             : [
                         'JTL: M{L: M¯L:M¼L- N%KÃN%K· N%K«N#K£ JXA§ JBABI¡AB F,AB EkABETA¡ A§K£ A¥K«A¥K· A¥KÃA²L- A¿L:B.L: ETL: EyL:E²L$ F%K³F.Ky G·E{ I{Ky I¯L:JTL:'
                       ],
      xMin           : 32,
      xMax           : 818,
      yMin           : 0,
      yMax           : 700,
      wdth           : 850
    };
    font["W"]        = {
      sC             : [
                         'E¯AB CuAB CNABC5AY B¿AqB»A± AkK« AkK± AkKÃAxL- A§L:A¹L: DoL: D¹L:E/L! EHK­ELKg E¯F¥ F£I@ F©ITFÀIh G4I{GXI{ HaI{ H§I{H¾Ih I2ITI8I@ J,F¥ JmKg JqK­J¬L! K!L:KJL: N!L: N4L:NAL- NNKÃNNK± NNK« LÃA± L¿AqL¦AY LkABLDAB J,AB I«ABIrAT IZAgITAy G¿DÁ FeAy F_AgFGAT F0ABE¯AB'
                       ],
      xMin           : 20,
      xMax           : 838,
      yMin           : 0,
      yMax           : 700,
      wdth           : 850
    };
    font["X"]        = {
      sC             : [
                         'JNF± M{A§ M£A}M£Am M£A]MtAO MgABMTAB J%AB IqABITAq GµC½ FTAq F6ABE¥AB BRAB B@ABB3AO B%A]B%Am B%A}B,A§ EgG( B]Ky BVK£BVK³ BVKÃBdL- BqL:B¥L: F2L: FgL:F©K« H!I« IJK¯ ImL:I¿L: MJL: M]L:MjL- MwKÃMwK³ MwK£MqKy JNF±'
                       ],
      xMin           : 50,
      xMax           : 799,
      yMin           : 0,
      yMax           : 700,
      wdth           : 850
    };
    font["Y"]        = {
      sC             : [
                         'JNL: MqL: M¥L:M²L- M¿KÃM¿Kµ M¿K§M¹Ky IÁE6 IÁAy IÁAcI±AR I¡ABIiAB F>AB F(ABE»AR E«AcE«Ay E«E6 AµKy A¯K§A¯Kµ A¯KÃA¼L- B%L:B8L: E_L: E¹L:F0K¯ GµH£ I{K« I»L:JNL:'
                       ],
      xMin           : 37,
      xMax           : 813,
      yMin           : 0,
      yMax           : 700,
      wdth           : 850
    };
    font["Z"]        = {
      sC             : [
                         'G½D] L»D] M.D]M>DL MND<MND% MNAy MNAcM>AR M.ABL»AB B·AB B¡ABBoAR B_AcB_Ay B_C» B_DFB}D_ GuHÃ C>HÃ C(HÃB»I0 B«I@B«IV B«K§ B«K½B»L* C(L:C>L: LoL: L§L:L·L* M#K½M#K§ M#Ig M#I<L­I# G½D]'
                       ],
      xMin           : 78,
      xMax           : 774,
      yMin           : 0,
      yMax           : 700,
      wdth           : 850
    };
    font["0"]        = {
      sC             : [
                         'M2H4 M2EH M2C>KzB6 J@A.G¶A. EgA.D.B6 BwC>BwEL BwH0 BwJ:D4KD EsLNG·LN J6LNKuKD M2J:M2H4'
                       ],
      hC             : [
                         ['FµHH FµE8 FµD£G8D[ G_D6G·D6 HJD6HqD] H¹D¥H¹E8 H¹HH H¹H£HrI$ HLIJG·IJ G]IJG7I# FµH¡FµHH']
                       ],
      xMin           : 90,
      xMax           : 760,
      yMin           : -10,
      yMax           : 710,
      wdth           : 850
    };
    font["1"]        = {
      sC             : [
                         'J]K§ J]D] L½D] M0D]M@DL MPD<MPD% MPAy MPAcM@AR M0ABL½AB CcAB CLABC<AR C,AcC,Ay C,D% C,D<C<DL CLD]CcD] FTD] FTH< D­G6 DyG*DfG* DRG*D>GF B¿IB B·IRB·Ih B·I}C.I³ FkL2 FyL:F­L: J%L: J<L:JLL* J]K½J]K§'
                       ],
      xMin           : 105,
      xMax           : 775,
      yMin           : 0,
      yMax           : 700,
      wdth           : 850
    };
    font["2"]        = {
      sC             : [
                         'HoD] L}D] LµD]M!DL M2D<M2D% M2Ay M2AcM!AR LµABL}AB CFAB C0ABBÃAR B³AcB³Ay B³Co B³D:CDDZ E»F0GAG0 HkH0HkHw HkH±HhHÀ HeI,HRI9 H@IFGÀIF G{IFGcI6 GJI%GBH¹ G:H©G2Hj G*HLG(HH FÃH2FqH2 CZH2 CFH2C7H@ C(HNC(He C(JDDWKI E©LNH,LN JRLNKiKZ L¡JgL¡H³ L¡H#L8GL KµF¿KpFx KLFRJ­EÃ ImE*HoD]'
                       ],
      xMin           : 103,
      xMax           : 760,
      yMin           : 0,
      yMax           : 710,
      wdth           : 850
    };
    font["3"]        = {
      sC             : [
                         'MPDy MPC*K°B, JJA.G½A. D£A.CHBw BiCaBiDN BiDcBvDo B¥D{B·D{ FJD{ FaD{FwDf F¯DPF¹DH G6D2G²D2 HiD2H±DP I4DoI4D¿ I4EuG½Eu E³Eu E{EuEkE§ EZE·EZF* EZGL EZG©E§G¿ H*I4 C{I4 CeI4CTID CDITCDIk CDK§ CDK½CTL* CeL:C{L: L>L: LTL:LeL* LuK½LuK§ LuI© LuIXLJI6 JRG¹ MPG<MPDy'
                       ],
      xMin           : 83,
      xMax           : 775,
      yMin           : -10,
      yMax           : 700,
      wdth           : 850
    };
    font["4"]        = {
      sC             : [
                         'K¥K§ K¥F8 M0F8 MHF8MXF( MiE»MiE¥ MiCX MiCBMXC2 MHC!M2C! K¥C! K¥Ay K¥AcKsAR KcABKLAB H:AB H#ABG·AR G§AcG§Ay G§C! B}C! BgC!BVC2 BFCBBFCX BFE£ BFF!B]F> G:K» GVL:G§L: KLL: KcL:KsL* K¥K½K¥K§'
                       ],
      hC             : [
                         ['F,F! GµF! GµH< F,F!']
                       ],
      xMin           : 66,
      xMax           : 787,
      yMin           : 0,
      yMax           : 700,
      wdth           : 850
    };
    font["5"]        = {
      sC             : [
                         'FyD{ G.D4G¡D4 HND4HxDU HÃDwHÃE0 HÃEkHwE­ HLF*G²F* GRF*G(Eµ F¡E{FuE{ CgE{ CTE{CGEª C:E·C:F! C:F0 C«Kg C¯K¯D%L" D@L:DgL: K¿L: L2L:LBL* LRK½LRK§ LRIk LRITLBID L2I4K¿I4 G!I4 FµH* G£HoIDHo J«HoKÃGk M8FgM8Dº M8CHK|B; J>A.G¬A. ETA.D!B0 BqC2BqDo BqD£B~D° B­D½B¿D½ F0D½ F_D½FyD{'
                       ],
      xMin           : 87,
      xMax           : 763,
      yMin           : -10,
      yMax           : 700,
      wdth           : 850
    };
    font["6"]        = {
      sC             : [
                         'J¿Ku I%HÃ I6I!IXI! I{I!J7Hº JuH¯K=Hj K©HFL@G³ L{GZLÁF¥ MBF*MBE> MBCVK¨BB JHA.H(A. E«A.DFB7 B§C@B§E* B§FuDJHm F»Kµ G4L:GgL: JyL: J­L:JºL- K#KÃK#K³ K#K£J¿Ku'
                       ],
      hC             : [
                         ['HÀDV INDyINE6 INEuHÀE¹ HmF8H%F8 GaF8G/E¹ F¡EuF¡E6 F¡DyG/DV GaD4H%D4 HmD4HÀDV']
                       ],
      xMin           : 97,
      xMax           : 768,
      yMin           : -10,
      yMax           : 700,
      wdth           : 850
    };
    font["7"]        = {
      sC             : [
                         'CgL: LiL: L£L:L²L* LÁK½LÁK§ LÁI_ LÁI%L§Hy HÁAq H§ABHLAB D­AB DyABDlAO D_A]D_Ak D_AyDcA£ HgHÃ CgHÃ CPHÃC@I0 C0I@C0IV C0K§ C0K½C@L* CPL:CgL:'
                       ],
      xMin           : 119,
      xMax           : 750,
      yMin           : 0,
      yMax           : 700,
      wdth           : 850
    };
    font["8"]        = {
      sC             : [
                         'B±I1 B±JVD6KR E_LNG¶LN JHLNKoKT L·JZL·I2 L·G­K«G% MHF@MHD~ MHC:K¶B4 J_A.G·A. EJA.CºB5 BeC<BeD¢ BeFBC¹G% B±G¯B±I1'
                       ],
      hC             : [
                         ['H}HA I(H_I(H± I(I>H}I] HPI{G·I{ GXI{G-I^ F¥I@F¥H² F¥H_G-HA GXH#G·H# HPH#H}HA','I4Dk I2DÃH§E@ HVEaG¸Ea GTEaG#E? FuDÁFuDk FuD6G"C½ GRC¡G·C¡ HVC¡H¨C¼ I4D4I4Dk']
                       ],
      xMin           : 81,
      xMax           : 771,
      yMin           : -10,
      yMax           : 710,
      wdth           : 850
    };
    font["9"]        = {
      sC             : [
                         'DgA« F¯D] F}DZFZDZ F8DZE|Df E>DqDvD¶ D,E6CsEm C8F!B·F{ BqGRBqH> BqJ%D-K: EkLNG­LN J*LNKmKE M.J<M.HR M.GoLªFª LaEÃKkD¹ H}Ak HZABH.AB D­AB DyABDlAO D_A]D_Am D_A}DgA«'
                       ],
      hC             : [
                         ['H¦Gi I4G­I4HH I4H©H¦I( HRIJG¯IJ GFIJF¸I( FeH©FeHH FeG­F¸Gi GFGFG¯GF HRGFH¦Gi']
                       ],
      xMin           : 87,
      xMax           : 758,
      yMin           : 0,
      yMax           : 710,
      wdth           : 850
    };
    font["!"]        = {
      sC             : [
                         'FJD£ I_D£ IuD£I§Dq I·DaI·DJ I·Ay I·AcI§AR IuABI_AB FJAB F4ABF#AR E·AcE·Ay E·DJ E·DaF#Dq F4D£FJD£',
                         'FJL: IaL: IwL:I©L* I¹K½I¹K£ I·E© I·EsI§Ed IuETI_ET FJET F2ETF"Ed E·EsE·E¯ E·K} E·K»F"L) F2L:FJL:'
                       ],
      xMin           : 297,
      xMax           : 554,
      yMin           : 0,
      yMax           : 700,
      wdth           : 850
    };
    font["|"]        = {
      sC             : [
                         'F§Ng I#Ng I:NgIJNV IZNFIZN0 IZ?N IZ?8IJ?( I:>»I#>» F§>» Fo>»F_?( FN?8FN?N FNN0 FNNFF_NV FoNgF§Ng'
                       ],
      xMin           : 326,
      xMax           : 524,
      yMin           : -149,
      yMax           : 850,
      wdth           : 850
    };
    font['"']        = {
      sC             : [
                         'I6LN L(LN L:LNLGLA LTL4LTL& LTK½LRK· J·H2 J}GsJ8Gs H8Gs H%GsG¼G¢ G¯G¯G¯GÁ G¯H! HVK£ HZKÁHvL6 H³LNI6LN',
                         'D}LN GoLN G£LNG°LA G½L4G½L& G½K½G»K· FZH2 FBGsE¡Gs C¡Gs CmGsC`G¢ CRG¯CRGÁ CRH! C¿K£ CÃKÁD;L6 DVLND}LN'
                       ],
      xMin           : 136,
      xMax           : 713,
      yMin           : 408,
      yMax           : 710,
      wdth           : 850
    };
    font["'"]        = {
      sC             : [
                         'F¿LP I±LP IÃLPJ-LC J:L6J:L) J:K¿J8K¹ H{H4 HcGuGÁGu EÁGu E¯GuE¢G¤ EsG±EsGÃ EsH# F<K¥ F@KÃF[L8 FwLPF¿LP'
                       ],
      xMin           : 280,
      xMax           : 572,
      yMin           : 409,
      yMax           : 711,
      wdth           : 850
    };
    font["#"]        = {
      sC             : [
                         'K¡J0 LuJ0 L­J0L»IÄ M%IµM%I¡ M%Gµ M%G¡L»Gp L­GaLuGa KLGa K6FH L@FH LVFHLeF9 LsF*LsE¹ LsC¹ LsC¥LeCt LVCeL@Ce J¥Ce JiB: JcA³J6A³ H:A³ H(A³G¾AÀ G±B*G±B< G±B@ H(Ce F¡Ce FeB: F_A³F2A³ D6A³ D#A³CºAÀ C­B*C­B< C­B@ D#Ce C6Ce C!CeB¶Ct B§C¥B§C¹ B§E¹ B§F*B¶F9 C!FHC6FH DXFH DoGa CkGa CVGaCGGp C8G¡C8Gµ C8I¡ C8IµCGIÄ CVJ0CkJ0 DÃJ0 E6K6 E>KaEgKa GcKa GuKaG¤KS G±KFG±K4 G±K. G{J0 I#J0 I:K6 IBKaIkKa KgKa KyKaK¨KS KµKFKµK4 KµK. K¡J0'
                       ],
      hC             : [
                         ['H{Gg GDGg G*FB HaFB H{Gg']
                       ],
      xMin           : 97,
      xMax           : 754,
      yMin           : 39,
      yMax           : 655,
      wdth           : 850
    };
    font["$"]        = {
      sC             : [
                         'H«H> KPG»L[G1 MgFJMgD° MgCPLRBV K>A]IFA: IF@: IF@#I6?· I%?§H³?§ F§?§ Fo?§F_?· FN@#FN@: FNA: DPAZCJBM BDC@BDDX BDD§BsD§ E¯D§ F6D§FPDk F«D4GµD4 IPD4IPDo IPDµH·E& HXE<G%EP BqE·BqH¥ BqJ,CqK" DqK½FNL> FNML FNMcF_Ms FoM¥F§M¥ H³M¥ I%M¥I6Ms IFMcIFML IFL> K(K¹L/JÂ M6J(M6I0 M6H¿M)H³ L¿H§L©H§ IXH§ I6H§H½H¿ HkIHG­IH F§IHF§H³ F§HsG8Hb GmHPH«H>'
                       ],
      xMin           : 65,
      xMax           : 786,
      yMin           : -95,
      yMax           : 800,
      wdth           : 850
    };
    font["%"]        = {
      sC             : [
                         'K2L: LZL: L­L:L­Kµ L­K©L¥K} E:Ai DÁABDsAB CJAB B½ABB½Ak B½AwC!A£ JkK· J©L:K2L:',
                         'N>D£ N>CD N>B<M_Aw L¡A0K_A0 J>A0I^Aw H}B<H}CF H}D¡ H}E«I`FN JBF·K^F· LyF·M[FM N>E©N>D£',
                         'G%J: G%H¡ G%GwFFG0 EgFkDFFk C%FkBEG0 AeGwAeH£ AeJ8 AeKBBGK« C*LNDELN EaLNFCKª G%K@G%J:'
                       ],
      hC             : [
                         [],
                         ['J¿D¥ J¿CF J¿C*K/B¹ KBB¥K_B¥ K{B¥K°B¹ KÃC*KÃCF KÃD¥ KÃDÁK°E1 K{EDK_ED KBEDK/E0 J¿D¿J¿D¥'],
                         ['C§J< C§H£ C§HeCºHP D*H<DFH< DcH<DvHP D«HeD«H£ D«J< D«JXDvJl DcJ¡DFJ¡ D*J¡CºJk C§JVC§J<']
                       ],
      xMin           : 17,
      xMax           : 830,
      yMin           : -9,
      yMax           : 710,
      wdth           : 850
    };
    font["&"]        = {
      sC             : [
                         'CeIi CeJyDiKe EmLPGYLP IFLPJAKg K<J}K<Ig K<G³IDF£ JFE{ JmE¿K!F­ K*FÁKJFÁ M±FÁ MÁFÁN(F· N2F­N2F{ N2F>MaE: L±D6LFCk N6A£ NBAuNBAf NBAVN7AL N,ABMÁAB JyAB J_ABJFAV I³A© HeA%F±A% D{A%CfA· BPB¥BPDT BPF>DTGP CeHXCeIi'
                       ],
      hC             : [
                         ['GaIÁ GDIÁG/I¬ F½IuF½IR F½I0GTH{ H(HÃH(I] H(IÁGaIÁ','E»Dq E»D:F@C¼ FiCyG%Cy GeCyG£C¯ FNE@ E»E%E»Dq']
                       ],
      xMin           : 71,
      xMax           : 832,
      yMin           : -14,
      yMax           : 711,
      wdth           : 850
    };
    font["("]        = {
      sC             : [
                         'H_H> H_C» H_B¿I$BQ ImA©J_Ay JmAwJyAk J§A_J§AL J§?0 J§>ÁJx>´ Jk>§JX>§ H<>§F£@. EDAXEDC¹ EDH@ EDJ¡F£L( H<MRJXMR JkMRJxME J§M8J§M% J§J­ J§JyJyJm JmJaJ_J_ ImJPI$I¨ H_I:H_H>'
                       ],
      xMin           : 257,
      xMax           : 609,
      yMin           : -159,
      yMax           : 776,
      wdth           : 850
    };
    font[")"]        = {
      sC             : [
                         'GJC» GJH> GJI:F¦I¨ F<JPEJJ_ E<JaE0Jm E#JyE#J­ E#M% E#M8E1ME E>MREPMR GmMRI(L( JeJ¡JeH@ JeC¹ JeAXI(@. Gm>§EP>§ E>>§E1>´ E#>ÁE#?0 E#AL E#A_E0Ak E<AwEJAy F<A©F¦BQ GJB¿GJC»'
                       ],
      xMin           : 241,
      xMax           : 593,
      yMin           : -159,
      yMax           : 776,
      wdth           : 850
    };
    font["*"]        = {
      sC             : [
                         'FaF¯ E.G¯ DÃG¹DÃH* DÃH>E.HL F:IR D±Ic DeIgDeI± DeIµDgI¿ D¿Ka E#K£EFK£ EPK£E]K{ F¥K8 FaLX F_LaF_Lk F_LuFlL¥ FyL³F­L³ I!L³ I4L³IAL¦ INLwINLk INL_ILLV I*K8 JPK{ J]K£JgK£ J«K£J³Ka KDI¿ KFIµKFI± KFIgJÁIc IqIR J}HN J¯H>J¯H* J¯G¹J}G­ ILF¯ I@F¥I*F¥ H·F¥H«F¿ G¹HN G#F¿ F½F¥F¦F¥ FmF¥FaF¯'
                       ],
      xMin           : 209,
      xMax           : 642,
      yMin           : 352,
      yMax           : 743,
      wdth           : 850
    };
    font["+"]        = {
      sC             : [
                         'FFB« FFE@ C«E@ CsE@CcEP CREaCREw CRG¯ CRH!CcH2 CsHBC«HB FFHB FFJq FFJ©FVJ¹ FgK%F}K% I,K% IBK%IRJ¹ IcJ©IcJq IcHB KÃHB L6HBLFH2 LVH!LVG¯ LVEw LVEaLFEP L6E@KÃE@ IcE@ IcB« IcBsIRBc IBBRI,BR F}BR FgBRFVBc FFBsFFB«'
                       ],
      xMin           : 136,
      xMax           : 714,
      yMin           : 72,
      yMax           : 626,
      wdth           : 850
    };
    font[","]        = {
      sC             : [
                         'F¿Dg I±Dg IÃDgJ-DY J:DLJ:D? J:D2J8D, H{@J Hc?­GÁ?­ EÁ?­ E¯?­E¢?º Es@#Es@6 Es@: F<C» F@D6F[DN FwDgF¿Dg'
                       ],
      xMin           : 280,
      xMax           : 572,
      yMin           : -92,
      yMax           : 210,
      wdth           : 850
    };
    font["-"]        = {
      sC             : [
                         'D¹GÁ J·GÁ K*GÁK:G± KJG¡KJGi KJEL KJE6K:E% K*D¹J·D¹ D¹D¹ D£D¹DqE% DaE6DaEL DaGi DaG¡DqG± D£GÁD¹GÁ'
                       ],
      xMin           : 207,
      xMax           : 644,
      yMin           : 234,
      yMax           : 430,
      wdth           : 850
    };
    font["."]        = {
      sC             : [
                         'FBDÁ IgDÁ I}DÁI¯D± I¿D¡I¿Di I¿Ay I¿AcI¯AR I}ABIgAB FBAB F,ABE¿AR E¯AcE¯Ay E¯Di E¯D¡E¿D± F,DÁFBDÁ'
                       ],
      xMin           : 293,
      xMax           : 557,
      yMin           : 0,
      yMax           : 238,
      wdth           : 850
    };
    font["/"]        = {
      sC             : [
                         'I£Mg KÁMg L0MgL=MY LJMLLJM? LJM2LFM( F{@V Fc?½F!?½ C­?½ Cy?½Cl@& C_@4C_@A C_@NCc@X I(M* I>MgI£Mg'
                       ],
      xMin           : 142,
      xMax           : 708,
      yMin           : -84,
      yMax           : 786,
      wdth           : 850
    };
    font[":"]        = {
      sC             : [
                         'FHD· I_D· IuD·I§D§ I·DuI·D_ I·Ay I·AcI§AR IuABI_AB FHAB F2ABF!AR EµAcEµAy EµD_ EµDuF!D§ F2D·FHD·',
                         'FHJ> I_J> IuJ>I§J. I·IÁI·I« I·G! I·F¯I§F} IuFmI_Fm FHFm F2FmF!F} EµF¯EµG! EµI« EµIÁF!J. F2J>FHJ>'
                       ],
      xMin           : 296,
      xMax           : 553,
      yMin           : 0,
      yMax           : 574,
      wdth           : 850
    };
    font[";"]        = {
      sC             : [
                         'F{Dg ImDg I¡DgI®DY I»DLI»D? I»D2I¹D, HX@J H@?­G}?­ E}?­ Ek?­E^?º EP@#EP@6 EP@: E½C» EÁD6F9DN FTDgF{Dg',
                         'FJJ< IaJ< IwJ<I©J, I¹I¿I¹I© I¹FÃ I¹F­I©F{ IwFkIaFk FJFk F4FkF#F{ E·F­E·FÃ E·I© E·I¿F#J, F4J<FJJ<'
                       ],
      xMin           : 263,
      xMax           : 555,
      yMin           : -92,
      yMax           : 573,
      wdth           : 850
    };
    font["<"]        = {
      sC             : [
                         'C¯FT C¯G( C¯GVDFG} J±K» J»KÁK&KÁ K6KÁKBKµ KNK©KNKu KNI0 KNH¯K>H¡ K.HqK*Ho J³HZJ±HZ H0F¡ J±E! J¹E!K+D² K@D}KBDw KND]KNDL KNA« KNAwKBAk K6A_K&A_ J»A_J±Ae DFE£ C¯F%C¯FT'
                       ],
      xMin           : 165,
      xMax           : 646,
      yMin           : 14,
      yMax           : 686,
      wdth           : 850
    };
    font["="]        = {
      sC             : [
                         'DmEÃ K>EÃ KTEÃKeE³ KuE£KuEk KuC¥ KuCmKeC] KTCLK>CL DmCL DVCLDFC] D6CmD6C¥ D6Ek D6E£DFE³ DVEÃDmEÃ',
                         'DmI· K>I· KTI·KeI§ KuIuKuI_ KuGw KuGaKeGP KTG@K>G@ DmG@ DVG@DFGP D6GaD6Gw D6I_ D6IuDFI§ DVI·DmI·'
                       ],
      xMin           : 186,
      xMax           : 665,
      yMin           : 133,
      yMax           : 553,
      wdth           : 850
    };
    font[">"]        = {
      sC             : [
                         'KÁG( KÁFT KÁF%KeE£ D¿Ae DµA_D¦A_ DuA_DiAk D]AwD]A« D]DL D]DqDmD¡ D}D¯D£D± D½E!D¿E! G{F¡ D¿HZ D·HZD¢Hn DkH£DgH© D]HÃD]I0 D]Ku D]K©DiKµ DuKÁD¦KÁ DµKÁD¿K» KeG} KÁGVKÁG('
                       ],
      xMin           : 205,
      xMax           : 686,
      yMin           : 14,
      yMax           : 686,
      wdth           : 850
    };
    font["?"]        = {
      sC             : [
                         'FRD£ IiD£ I¡D£I±Dq IÁDaIÁDJ IÁAy IÁAcI±AR I¡ABIiAB FRAB F<ABF,AR E¿AcE¿Ay E¿DJ E¿DaF,Dq F<D£FRD£',
                         'D%K6 DwK{E¡L# F©LNH5LN IeLNJhL% KkK¡L*K@ M!JLM!IV M!H©LzH? LPGyKÀGM KkG!K+Fm JmF6JUE· J>EsJ8Ek J#ETI«ET F4ET F%ETE½E` E±EkE±Ew E±E¥E³E« F#FFF_F¥ F»G>GRGe HiHFHiH} HiIFG³IF G_IFGDI3 G*HÃG$H« FÃHqF¯H^ FyHJFTHJ C8HJ C!HJB·H] B©HoB©H© B©H« B©J,D%K6'
                       ],
      xMin           : 98,
      xMax           : 752,
      yMin           : 0,
      yMax           : 710,
      wdth           : 850
    };
    font["@"]        = {
      sC             : [
                         'E0D5 DgD½DgFi DgH6E0H¿ E{I¥G%I¥ GµI¥HFIa HXITHkID H}I4 H}I< H}IRH¯Ic H¿IsI2Is JkIs J£IsJ³Ic JÃIRJÃI< JÃE· JÃE*KgE* K·E*L!EE L0EaL0E· L0G¥ L0IeK,J@ J(J¿H4J¿ FFJ¿E%JH DTI¿D%II CyHwCyG§ CyEo CyB4H2B4 IXB4I¿BA JaBNJ©Bm K,B­K>B­ MkB­ M}B­M«B¡ M·BsM·Bc M·BRM­BD M#ABK©@~ Ji@8H6@8 D­@8C0At AVC.AVEi AVG§ AVJ*C1Kb D¯L»H1L» KVL»L·Ke NRJ0NRG¡ NRE¹ NRD£MoCÄ L­CBKZCB IwCBI2D2 HgCPG0CP E{CPE0D5'
                       ],
      hC             : [
                         ['G£G§ GNG§G=G[ G,G2G,Fj G,EÃG=E| GNEVG£EV H_EVH_Fn H_G§G£G§']
                       ],
      xMin           : 10,
      xMax           : 840,
      yMin           : -69,
      yMax           : 747,
      wdth           : 850
    };
    font["["]        = {
      sC             : [
                         'EBMP JÃMP K6MPKFM@ KVM0KVL½ KVK, KVJ¹KFJ© K6JwJÃJw G·Jw G·A_ JÃA_ K6A_KFAN KVA>KVA( KV?: KV?#KF>· K6>§JÃ>§ EB>§ E,>§D¿>· D¯?#D¯?: D¯L½ D¯M0D¿M@ E,MPEBMP'
                       ],
      xMin           : 229,
      xMax           : 650,
      yMin           : -159,
      yMax           : 775,
      wdth           : 850
    };
    font["]"]        = {
      sC             : [
                         'D«MP JgMP J}MPJ¯M@ J¿M0J¿L½ J¿?: J¿?#J¯>· J}>§Jg>§ D«>§ Ds>§Dc>· DR?#DR?: DRA( DRA>DcAN DsA_D«A_ G·A_ G·Jw D«Jw DsJwDcJ© DRJ¹DRK, DRL½ DRM0DcM@ DsMPD«MP'
                       ],
      xMin           : 200,
      xMax           : 621,
      yMin           : -159,
      yMax           : 775,
      wdth           : 850
    };
    font["^"]        = {
      sC             : [
                         'FRLy E0Ly D³LyD³L» D³M*DÃM4 FFN@ F}NgG#Ng H§Ng I,NgIcN@ J«M4 J»M*J»L» J»LyJyLy IVLy I2LyH©L© G·M2 G!L© FwLyFRLy'
                       ],
      xMin           : 231,
      xMax           : 619,
      yMin           : 731,
      yMax           : 850,
      wdth           : 850
    };
    font["_"]        = {
      sC             : [
                         'BRB¿ MTB¿ MkB¿M{B¯ M­B}M­Bg M­@V M­@@M{@0 Mk?ÃMT?Ã BR?Ã B<?ÃB,@0 A¿@@A¿@V A¿Bg A¿B}B,B¯ B<B¿BRB¿'
                       ],
      xMin           : 45,
      xMax           : 804,
      yMin           : -81,
      yMax           : 109,
      wdth           : 850
    };
    font[" "]        = {
      sC             : [
                       ],
      xMin           : 10000,
      xMax           : -10000,
      yMin           : 10000,
      yMax           : -10000,
      wdth           : 850
    };
    font[" "]        = {
      sC             : [
                       ],
      xMin           : 10000,
      xMax           : -10000,
      yMin           : 10000,
      yMax           : -10000,
      wdth           : 850
    };

    return font;
  }
}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
      __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = earcut;
module.exports.default = earcut;

function earcut(data, holeIndices, dim) {

  dim = dim || 2;

  var hasHoles = holeIndices && holeIndices.length,
      outerLen = hasHoles ? holeIndices[0] * dim : data.length,
      outerNode = linkedList(data, 0, outerLen, dim, true),
      triangles = [];

  if (!outerNode || outerNode.next === outerNode.prev) return triangles;

  var minX, minY, maxX, maxY, x, y, invSize;

  if (hasHoles) outerNode = eliminateHoles(data, holeIndices, outerNode, dim);

  // if the shape is not too simple, we'll use z-order curve hash later; calculate polygon bbox
  if (data.length > 80 * dim) {
      minX = maxX = data[0];
      minY = maxY = data[1];

      for (var i = dim; i < outerLen; i += dim) {
          x = data[i];
          y = data[i + 1];
          if (x < minX) minX = x;
          if (y < minY) minY = y;
          if (x > maxX) maxX = x;
          if (y > maxY) maxY = y;
      }

      // minX, minY and invSize are later used to transform coords into integers for z-order calculation
      invSize = Math.max(maxX - minX, maxY - minY);
      invSize = invSize !== 0 ? 32767 / invSize : 0;
  }

  earcutLinked(outerNode, triangles, dim, minX, minY, invSize, 0);

  return triangles;
}

// create a circular doubly linked list from polygon points in the specified winding order
function linkedList(data, start, end, dim, clockwise) {
  var i, last;

  if (clockwise === (signedArea(data, start, end, dim) > 0)) {
      for (i = start; i < end; i += dim) last = insertNode(i, data[i], data[i + 1], last);
  } else {
      for (i = end - dim; i >= start; i -= dim) last = insertNode(i, data[i], data[i + 1], last);
  }

  if (last && equals(last, last.next)) {
      removeNode(last);
      last = last.next;
  }

  return last;
}

// eliminate colinear or duplicate points
function filterPoints(start, end) {
  if (!start) return start;
  if (!end) end = start;

  var p = start,
      again;
  do {
      again = false;

      if (!p.steiner && (equals(p, p.next) || area(p.prev, p, p.next) === 0)) {
          removeNode(p);
          p = end = p.prev;
          if (p === p.next) break;
          again = true;

      } else {
          p = p.next;
      }
  } while (again || p !== end);

  return end;
}

// main ear slicing loop which triangulates a polygon (given as a linked list)
function earcutLinked(ear, triangles, dim, minX, minY, invSize, pass) {
  if (!ear) return;

  // interlink polygon nodes in z-order
  if (!pass && invSize) indexCurve(ear, minX, minY, invSize);

  var stop = ear,
      prev, next;

  // iterate through ears, slicing them one by one
  while (ear.prev !== ear.next) {
      prev = ear.prev;
      next = ear.next;

      if (invSize ? isEarHashed(ear, minX, minY, invSize) : isEar(ear)) {
          // cut off the triangle
          triangles.push(prev.i / dim | 0);
          triangles.push(ear.i / dim | 0);
          triangles.push(next.i / dim | 0);

          removeNode(ear);

          // skipping the next vertex leads to less sliver triangles
          ear = next.next;
          stop = next.next;

          continue;
      }

      ear = next;

      // if we looped through the whole remaining polygon and can't find any more ears
      if (ear === stop) {
          // try filtering points and slicing again
          if (!pass) {
              earcutLinked(filterPoints(ear), triangles, dim, minX, minY, invSize, 1);

          // if this didn't work, try curing all small self-intersections locally
          } else if (pass === 1) {
              ear = cureLocalIntersections(filterPoints(ear), triangles, dim);
              earcutLinked(ear, triangles, dim, minX, minY, invSize, 2);

          // as a last resort, try splitting the remaining polygon into two
          } else if (pass === 2) {
              splitEarcut(ear, triangles, dim, minX, minY, invSize);
          }

          break;
      }
  }
}

// check whether a polygon node forms a valid ear with adjacent nodes
function isEar(ear) {
  var a = ear.prev,
      b = ear,
      c = ear.next;

  if (area(a, b, c) >= 0) return false; // reflex, can't be an ear

  // now make sure we don't have other points inside the potential ear
  var ax = a.x, bx = b.x, cx = c.x, ay = a.y, by = b.y, cy = c.y;

  // triangle bbox; min & max are calculated like this for speed
  var x0 = ax < bx ? (ax < cx ? ax : cx) : (bx < cx ? bx : cx),
      y0 = ay < by ? (ay < cy ? ay : cy) : (by < cy ? by : cy),
      x1 = ax > bx ? (ax > cx ? ax : cx) : (bx > cx ? bx : cx),
      y1 = ay > by ? (ay > cy ? ay : cy) : (by > cy ? by : cy);

  var p = c.next;
  while (p !== a) {
      if (p.x >= x0 && p.x <= x1 && p.y >= y0 && p.y <= y1 &&
          pointInTriangle(ax, ay, bx, by, cx, cy, p.x, p.y) &&
          area(p.prev, p, p.next) >= 0) return false;
      p = p.next;
  }

  return true;
}

function isEarHashed(ear, minX, minY, invSize) {
  var a = ear.prev,
      b = ear,
      c = ear.next;

  if (area(a, b, c) >= 0) return false; // reflex, can't be an ear

  var ax = a.x, bx = b.x, cx = c.x, ay = a.y, by = b.y, cy = c.y;

  // triangle bbox; min & max are calculated like this for speed
  var x0 = ax < bx ? (ax < cx ? ax : cx) : (bx < cx ? bx : cx),
      y0 = ay < by ? (ay < cy ? ay : cy) : (by < cy ? by : cy),
      x1 = ax > bx ? (ax > cx ? ax : cx) : (bx > cx ? bx : cx),
      y1 = ay > by ? (ay > cy ? ay : cy) : (by > cy ? by : cy);

  // z-order range for the current triangle bbox;
  var minZ = zOrder(x0, y0, minX, minY, invSize),
      maxZ = zOrder(x1, y1, minX, minY, invSize);

  var p = ear.prevZ,
      n = ear.nextZ;

  // look for points inside the triangle in both directions
  while (p && p.z >= minZ && n && n.z <= maxZ) {
      if (p.x >= x0 && p.x <= x1 && p.y >= y0 && p.y <= y1 && p !== a && p !== c &&
          pointInTriangle(ax, ay, bx, by, cx, cy, p.x, p.y) && area(p.prev, p, p.next) >= 0) return false;
      p = p.prevZ;

      if (n.x >= x0 && n.x <= x1 && n.y >= y0 && n.y <= y1 && n !== a && n !== c &&
          pointInTriangle(ax, ay, bx, by, cx, cy, n.x, n.y) && area(n.prev, n, n.next) >= 0) return false;
      n = n.nextZ;
  }

  // look for remaining points in decreasing z-order
  while (p && p.z >= minZ) {
      if (p.x >= x0 && p.x <= x1 && p.y >= y0 && p.y <= y1 && p !== a && p !== c &&
          pointInTriangle(ax, ay, bx, by, cx, cy, p.x, p.y) && area(p.prev, p, p.next) >= 0) return false;
      p = p.prevZ;
  }

  // look for remaining points in increasing z-order
  while (n && n.z <= maxZ) {
      if (n.x >= x0 && n.x <= x1 && n.y >= y0 && n.y <= y1 && n !== a && n !== c &&
          pointInTriangle(ax, ay, bx, by, cx, cy, n.x, n.y) && area(n.prev, n, n.next) >= 0) return false;
      n = n.nextZ;
  }

  return true;
}

// go through all polygon nodes and cure small local self-intersections
function cureLocalIntersections(start, triangles, dim) {
  var p = start;
  do {
      var a = p.prev,
          b = p.next.next;

      if (!equals(a, b) && intersects(a, p, p.next, b) && locallyInside(a, b) && locallyInside(b, a)) {

          triangles.push(a.i / dim | 0);
          triangles.push(p.i / dim | 0);
          triangles.push(b.i / dim | 0);

          // remove two nodes involved
          removeNode(p);
          removeNode(p.next);

          p = start = b;
      }
      p = p.next;
  } while (p !== start);

  return filterPoints(p);
}

// try splitting polygon into two and triangulate them independently
function splitEarcut(start, triangles, dim, minX, minY, invSize) {
  // look for a valid diagonal that divides the polygon into two
  var a = start;
  do {
      var b = a.next.next;
      while (b !== a.prev) {
          if (a.i !== b.i && isValidDiagonal(a, b)) {
              // split the polygon in two by the diagonal
              var c = splitPolygon(a, b);

              // filter colinear points around the cuts
              a = filterPoints(a, a.next);
              c = filterPoints(c, c.next);

              // run earcut on each half
              earcutLinked(a, triangles, dim, minX, minY, invSize, 0);
              earcutLinked(c, triangles, dim, minX, minY, invSize, 0);
              return;
          }
          b = b.next;
      }
      a = a.next;
  } while (a !== start);
}

// link every hole into the outer loop, producing a single-ring polygon without holes
function eliminateHoles(data, holeIndices, outerNode, dim) {
  var queue = [],
      i, len, start, end, list;

  for (i = 0, len = holeIndices.length; i < len; i++) {
      start = holeIndices[i] * dim;
      end = i < len - 1 ? holeIndices[i + 1] * dim : data.length;
      list = linkedList(data, start, end, dim, false);
      if (list === list.next) list.steiner = true;
      queue.push(getLeftmost(list));
  }

  queue.sort(compareX);

  // process holes from left to right
  for (i = 0; i < queue.length; i++) {
      outerNode = eliminateHole(queue[i], outerNode);
  }

  return outerNode;
}

function compareX(a, b) {
  return a.x - b.x;
}

// find a bridge between vertices that connects hole with an outer ring and and link it
function eliminateHole(hole, outerNode) {
  var bridge = findHoleBridge(hole, outerNode);
  if (!bridge) {
      return outerNode;
  }

  var bridgeReverse = splitPolygon(bridge, hole);

  // filter collinear points around the cuts
  filterPoints(bridgeReverse, bridgeReverse.next);
  return filterPoints(bridge, bridge.next);
}

// David Eberly's algorithm for finding a bridge between hole and outer polygon
function findHoleBridge(hole, outerNode) {
  var p = outerNode,
      hx = hole.x,
      hy = hole.y,
      qx = -Infinity,
      m;

  // find a segment intersected by a ray from the hole's leftmost point to the left;
  // segment's endpoint with lesser x will be potential connection point
  do {
      if (hy <= p.y && hy >= p.next.y && p.next.y !== p.y) {
          var x = p.x + (hy - p.y) * (p.next.x - p.x) / (p.next.y - p.y);
          if (x <= hx && x > qx) {
              qx = x;
              m = p.x < p.next.x ? p : p.next;
              if (x === hx) return m; // hole touches outer segment; pick leftmost endpoint
          }
      }
      p = p.next;
  } while (p !== outerNode);

  if (!m) return null;

  // look for points inside the triangle of hole point, segment intersection and endpoint;
  // if there are no points found, we have a valid connection;
  // otherwise choose the point of the minimum angle with the ray as connection point

  var stop = m,
      mx = m.x,
      my = m.y,
      tanMin = Infinity,
      tan;

  p = m;

  do {
      if (hx >= p.x && p.x >= mx && hx !== p.x &&
              pointInTriangle(hy < my ? hx : qx, hy, mx, my, hy < my ? qx : hx, hy, p.x, p.y)) {

          tan = Math.abs(hy - p.y) / (hx - p.x); // tangential

          if (locallyInside(p, hole) &&
              (tan < tanMin || (tan === tanMin && (p.x > m.x || (p.x === m.x && sectorContainsSector(m, p)))))) {
              m = p;
              tanMin = tan;
          }
      }

      p = p.next;
  } while (p !== stop);

  return m;
}

// whether sector in vertex m contains sector in vertex p in the same coordinates
function sectorContainsSector(m, p) {
  return area(m.prev, m, p.prev) < 0 && area(p.next, m, m.next) < 0;
}

// interlink polygon nodes in z-order
function indexCurve(start, minX, minY, invSize) {
  var p = start;
  do {
      if (p.z === 0) p.z = zOrder(p.x, p.y, minX, minY, invSize);
      p.prevZ = p.prev;
      p.nextZ = p.next;
      p = p.next;
  } while (p !== start);

  p.prevZ.nextZ = null;
  p.prevZ = null;

  sortLinked(p);
}

// Simon Tatham's linked list merge sort algorithm
// http://www.chiark.greenend.org.uk/~sgtatham/algorithms/listsort.html
function sortLinked(list) {
  var i, p, q, e, tail, numMerges, pSize, qSize,
      inSize = 1;

  do {
      p = list;
      list = null;
      tail = null;
      numMerges = 0;

      while (p) {
          numMerges++;
          q = p;
          pSize = 0;
          for (i = 0; i < inSize; i++) {
              pSize++;
              q = q.nextZ;
              if (!q) break;
          }
          qSize = inSize;

          while (pSize > 0 || (qSize > 0 && q)) {

              if (pSize !== 0 && (qSize === 0 || !q || p.z <= q.z)) {
                  e = p;
                  p = p.nextZ;
                  pSize--;
              } else {
                  e = q;
                  q = q.nextZ;
                  qSize--;
              }

              if (tail) tail.nextZ = e;
              else list = e;

              e.prevZ = tail;
              tail = e;
          }

          p = q;
      }

      tail.nextZ = null;
      inSize *= 2;

  } while (numMerges > 1);

  return list;
}

// z-order of a point given coords and inverse of the longer side of data bbox
function zOrder(x, y, minX, minY, invSize) {
  // coords are transformed into non-negative 15-bit integer range
  x = (x - minX) * invSize | 0;
  y = (y - minY) * invSize | 0;

  x = (x | (x << 8)) & 0x00FF00FF;
  x = (x | (x << 4)) & 0x0F0F0F0F;
  x = (x | (x << 2)) & 0x33333333;
  x = (x | (x << 1)) & 0x55555555;

  y = (y | (y << 8)) & 0x00FF00FF;
  y = (y | (y << 4)) & 0x0F0F0F0F;
  y = (y | (y << 2)) & 0x33333333;
  y = (y | (y << 1)) & 0x55555555;

  return x | (y << 1);
}

// find the leftmost node of a polygon ring
function getLeftmost(start) {
  var p = start,
      leftmost = start;
  do {
      if (p.x < leftmost.x || (p.x === leftmost.x && p.y < leftmost.y)) leftmost = p;
      p = p.next;
  } while (p !== start);

  return leftmost;
}

// check if a point lies within a convex triangle
function pointInTriangle(ax, ay, bx, by, cx, cy, px, py) {
  return (cx - px) * (ay - py) >= (ax - px) * (cy - py) &&
         (ax - px) * (by - py) >= (bx - px) * (ay - py) &&
         (bx - px) * (cy - py) >= (cx - px) * (by - py);
}

// check if a diagonal between two polygon nodes is valid (lies in polygon interior)
function isValidDiagonal(a, b) {
  return a.next.i !== b.i && a.prev.i !== b.i && !intersectsPolygon(a, b) && // dones't intersect other edges
         (locallyInside(a, b) && locallyInside(b, a) && middleInside(a, b) && // locally visible
          (area(a.prev, a, b.prev) || area(a, b.prev, b)) || // does not create opposite-facing sectors
          equals(a, b) && area(a.prev, a, a.next) > 0 && area(b.prev, b, b.next) > 0); // special zero-length case
}

// signed area of a triangle
function area(p, q, r) {
  return (q.y - p.y) * (r.x - q.x) - (q.x - p.x) * (r.y - q.y);
}

// check if two points are equal
function equals(p1, p2) {
  return p1.x === p2.x && p1.y === p2.y;
}

// check if two segments intersect
function intersects(p1, q1, p2, q2) {
  var o1 = sign(area(p1, q1, p2));
  var o2 = sign(area(p1, q1, q2));
  var o3 = sign(area(p2, q2, p1));
  var o4 = sign(area(p2, q2, q1));

  if (o1 !== o2 && o3 !== o4) return true; // general case

  if (o1 === 0 && onSegment(p1, p2, q1)) return true; // p1, q1 and p2 are collinear and p2 lies on p1q1
  if (o2 === 0 && onSegment(p1, q2, q1)) return true; // p1, q1 and q2 are collinear and q2 lies on p1q1
  if (o3 === 0 && onSegment(p2, p1, q2)) return true; // p2, q2 and p1 are collinear and p1 lies on p2q2
  if (o4 === 0 && onSegment(p2, q1, q2)) return true; // p2, q2 and q1 are collinear and q1 lies on p2q2

  return false;
}

// for collinear points p, q, r, check if point q lies on segment pr
function onSegment(p, q, r) {
  return q.x <= Math.max(p.x, r.x) && q.x >= Math.min(p.x, r.x) && q.y <= Math.max(p.y, r.y) && q.y >= Math.min(p.y, r.y);
}

function sign(num) {
  return num > 0 ? 1 : num < 0 ? -1 : 0;
}

// check if a polygon diagonal intersects any polygon segments
function intersectsPolygon(a, b) {
  var p = a;
  do {
      if (p.i !== a.i && p.next.i !== a.i && p.i !== b.i && p.next.i !== b.i &&
              intersects(p, p.next, a, b)) return true;
      p = p.next;
  } while (p !== a);

  return false;
}

// check if a polygon diagonal is locally inside the polygon
function locallyInside(a, b) {
  return area(a.prev, a, a.next) < 0 ?
      area(a, b, a.next) >= 0 && area(a, a.prev, b) >= 0 :
      area(a, b, a.prev) < 0 || area(a, a.next, b) < 0;
}

// check if the middle point of a polygon diagonal is inside the polygon
function middleInside(a, b) {
  var p = a,
      inside = false,
      px = (a.x + b.x) / 2,
      py = (a.y + b.y) / 2;
  do {
      if (((p.y > py) !== (p.next.y > py)) && p.next.y !== p.y &&
              (px < (p.next.x - p.x) * (py - p.y) / (p.next.y - p.y) + p.x))
          inside = !inside;
      p = p.next;
  } while (p !== a);

  return inside;
}

// link two polygon vertices with a bridge; if the vertices belong to the same ring, it splits polygon into two;
// if one belongs to the outer ring and another to a hole, it merges it into a single ring
function splitPolygon(a, b) {
  var a2 = new Node(a.i, a.x, a.y),
      b2 = new Node(b.i, b.x, b.y),
      an = a.next,
      bp = b.prev;

  a.next = b;
  b.prev = a;

  a2.next = an;
  an.prev = a2;

  b2.next = a2;
  a2.prev = b2;

  bp.next = b2;
  b2.prev = bp;

  return b2;
}

// create a node and optionally link it with previous one (in a circular doubly linked list)
function insertNode(i, x, y, last) {
  var p = new Node(i, x, y);

  if (!last) {
      p.prev = p;
      p.next = p;

  } else {
      p.next = last.next;
      p.prev = last;
      last.next.prev = p;
      last.next = p;
  }
  return p;
}

function removeNode(p) {
  p.next.prev = p.prev;
  p.prev.next = p.next;

  if (p.prevZ) p.prevZ.nextZ = p.nextZ;
  if (p.nextZ) p.nextZ.prevZ = p.prevZ;
}

function Node(i, x, y) {
  // vertex index in coordinates array
  this.i = i;

  // vertex coordinates
  this.x = x;
  this.y = y;

  // previous and next vertex nodes in a polygon ring
  this.prev = null;
  this.next = null;

  // z-order curve value
  this.z = 0;

  // previous and next nodes in z-order
  this.prevZ = null;
  this.nextZ = null;

  // indicates whether this is a steiner point
  this.steiner = false;
}

// return a percentage difference between the polygon area and its triangulation area;
// used to verify correctness of triangulation
earcut.deviation = function (data, holeIndices, dim, triangles) {
  var hasHoles = holeIndices && holeIndices.length;
  var outerLen = hasHoles ? holeIndices[0] * dim : data.length;

  var polygonArea = Math.abs(signedArea(data, 0, outerLen, dim));
  if (hasHoles) {
      for (var i = 0, len = holeIndices.length; i < len; i++) {
          var start = holeIndices[i] * dim;
          var end = i < len - 1 ? holeIndices[i + 1] * dim : data.length;
          polygonArea -= Math.abs(signedArea(data, start, end, dim));
      }
  }

  var trianglesArea = 0;
  for (i = 0; i < triangles.length; i += 3) {
      var a = triangles[i] * dim;
      var b = triangles[i + 1] * dim;
      var c = triangles[i + 2] * dim;
      trianglesArea += Math.abs(
          (data[a] - data[c]) * (data[b + 1] - data[a + 1]) -
          (data[a] - data[b]) * (data[c + 1] - data[a + 1]));
  }

  return polygonArea === 0 && trianglesArea === 0 ? 0 :
      Math.abs((trianglesArea - polygonArea) / polygonArea);
};

function signedArea(data, start, end, dim) {
  var sum = 0;
  for (var i = start, j = end - dim; i < end; i += dim) {
      sum += (data[j] - data[i]) * (data[i + 1] + data[j + 1]);
      j = i;
  }
  return sum;
}

// turn a polygon in a multi-dimensional array form (e.g. as in GeoJSON) into a form Earcut accepts
earcut.flatten = function (data) {
  var dim = data[0][0].length,
      result = {vertices: [], holes: [], dimensions: dim},
      holeIndex = 0;

  for (var i = 0; i < data.length; i++) {
      for (var j = 0; j < data[i].length; j++) {
          for (var d = 0; d < dim; d++) result.vertices.push(data[i][j][d]);
      }
      if (i > 0) {
          holeIndex += data[i - 1].length;
          result.holes.push(holeIndex);
      }
  }
  return result;
};


/***/ })
/******/ ]);