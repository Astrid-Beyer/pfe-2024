# Projet de fin d'études M2 Informatique GIG 2023-2024

## Génération point-de-vue dépendante de formes anamorphiques 3D par usinage virtuel

Un sujet proposé par [Jean-Luc MARI](https://cv.hal.science/jean-luc-mari).

L’objectif de ce projet est de développer un **outil de génération de formes anamorphiques 3D** dans
un premier temps, et de les préparer pour une **impression 3D** dans un second temps.

Ces formes révèlent leur véritable apparence lorsqu'elles sont observées sous un certain angle.  
L’idée est ici d’élaborer une approche de génération de formes s’appuyant sur des techniques d’usinage virtuel, de sorte à faire une “extrusion inverse” de la forme pour que deux voire trois points de vues puissent permettre des rendus différents, en fonction de la position de
l’observateur.

Une fois une approche stable et performante élaborée, il sera question de fabriquer des formes
générées avec une imprimante 3D et d’étudier les problèmes qui pourraient se poser (difficultés à
fabriquer certaines formes avec un dévers ou une voute par exemple).

## Environnements

- Vue
- Babylon

## Références

- [SketchUp Community - Need help in forming anamorphic 34D shapes using...](https://forums.sketchup.com/t/need-help-in-forming-anamorphic-3d-shapes-using-sketchup-make-2017/136839)
- [YouTube: 3DPrintingGeek - How to Design Anamorphic Text](https://www.youtube.com/watch?v=6uTn26dEoF8)

## Captures

## Setup du projet

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Type-Check, Compile and Minify for Production

```sh
npm run build
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```

## Recommendations pour l'IDE

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (et désactiver Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin).

## Auteurs

Jeff ALLARD  
Astrid BEYER  
Florian MERNIZ  